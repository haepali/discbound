# Discbound

## Introduction

This tool is designed to allow generation of printable booklets for use 
primarily in the discbound system, however, it is suitable for most binding 
systems (e.g. hole punched, sprial bound, etc.)

## Dependencies and Setup

The following dependencies are needed in order to run the tool:

- Linux
- Python 3.5 or later
- Inkscape
- pdfmerge

In addition, the following Python packages are necessary:

- svgwrite

Depending on the booklets being generated, you'll likely need to install some
fonts. To install fonts that are used by default for some booklets, run the 
following script:

    python3 .\scripts\getfonts.py

Fonts will be installed to the users ~/.fonts directory.


## Design

The discbound generator is currently straddling an awkward gap between a static
website generator, and GUI prototyping engine, and full fledged HTML/SVG (albeit
in JSON instead).

The intent of the design is to allow new booklets to be auto-generated quickly
and with a large variety of customizations, and to be able to allow users to 
do it themselves. A few examples to illustrate this:

  * Generate a single booklet with different page sizes *without* awkward
    scaling issues.

  * Automatically layout pages on larger sheets to avoid waste, allow easy
    cutting, double sided printing, and injecting blank pages where necessary.

  * Allow precise alignment of elements such as guaranteeing a grid is aligned
    to an inch.

  * Accounting for printer margins.

  * Allow re-generating all calendars in a new language.

  * Allow injecting region specific holidays, events, or birthdays in all
    calendars.

To achieve this, SVG was used since elements can be aligned using precise units.
From SVG, the booklets will be rendered to PDFs throuhg a pipeline which first
uses inkscape to render a single page to a PDF, and then uses pdfmerge to 
combine the single page PDF's into a full booklet.

Additionally, by using SVG's, hand made content can eventually be included, and
thumbnails can be easily added to webpages for previewing.



## Structure

### Current Design

The current and future designs rely almost exclusively on JSON to provide the 
configuration and parameters for the booklets, pages, and elements. While some 
parameters may be auto-filled during generation (e.g. calculating the size of
the body elements, the sheet-layout signature, etc.), a fully detailed JSON
configuration should be passed through the stack.

Generating is a two step process. First the elements are created / loaded, then
the resulting SVG is generated.

Note: to understand how to build up a configuration, it is recommended to look 
at the JSON test artifacts in tests/json_test_files. Additionally, you may look
at the Param class definitions for each content generator in discbound/contents.
In the future, these parameters should be exposed dynamically through a JSON 
schema or some other similar mechanism.


**Page**

The model is built around a page as the base unit (breaking new ground here). 
Currently, a page can have a header, footer, and body elements. The heights for
the header and footer can be specified independently, and the body will fill up
the remaining space. 

Currently, if the user wants a body element that is smaller than the fill space,
then it either needs to be a configurable part of that specific element, or a 
new element generator needs to be developed. See the ``Future Designs`` section
below for future plans on how to handle this.


**Content Items (Elements)**

Below a page, we have content items. A content item is a single component on a 
page and is typically used to fill up the entire header, footer, or body 
elements. For example, we might have a dot-grid element which has its own 
configuration parameters, and can be set in any of the header, footer, or body.
Additionally, there's nothing stopping an element from making use of other 
elements in a sub/nested manner.

Named params / named param groups should be provided where appropriate. Named
params are simply one or more parameters to an element which have a pre-defined
alias to access them. For example instead of manually specifying spacing of 
0.25" on a quad-ruled grid, the user can just specify "quad" as the named param.


**Sheet**

A sheet is the next level up and consists of multiple pages layed out next to 
each other. Sheets themselves are not directly configured by the user; instead,
a booklet model is used. Based on the layout of the booklet for printing and
cutting, pages may be mapped differently to different sheets. A sheet is treated
as being single sided and is the base unit that will rendered to a PDF. Duplex 
or two-sided printing is handled by the printer / printer drivers and requires
the entire booklet to be merged together into a single PDF.


**Booklets**

The next layer above a sheet is a booklet. A booklet is simply a collection of 
pages to be printed out, along with some parameters for configuring and laying
out the sheets. The layout may change based on what is desired for reviewing, 
printing, cutting, etc.

Along with pages, booklets are the only layer which has a dedicated 
configuration file. The booklet configuration ultimately contains the entire 
page configuration nested within. 

There are actually 3 separate types of booklets, each one corresponds to a 
different level of flexibility. 

  * ``Booklet`` is the base model and requires the entire booklet to be 
    specified in the JSON config. While this allows the most flexibility, it is 
    also the most manual.

  * ``BookletTemplateNamedParams`` allows defining and expanding the pages by 
    using combinations of named parameter groups. This allows generating all 
    possible combinations of pre-defined parameter groups for a given element.
    This is restricted to generating a booklet of a single page style.

  * ``BookletTemplateNamed`` allows automating the generation of more complex
    booklets than ``BookletTemplateNamedParams`` does. However, this requires
    additional generators to be defined simliar to how element generators work.

When generating a booklet, several parameters are used to name the output files.
The parameters chosen are the ones believed to be most likely to change, or be
quickly found when searching. This includes things like page size, sheet size, 
etc.



### Future Designs

**New Booklet Templates**

The current booklet model is to take a sparse list of parameters and generate
the booklet / page configurations dynamicall from that. This will be updated to
use a "template" JSON configuration. Python string Templates will be used to 
auto-fill any variables necessary.

**Infinitely Divisible Pages**

Going forward, the static heder, footer, body model will likely be discarded. 
Instead, users will have the ability to divide the entire page up in a nested 
manner. This will allow more complex elements to be composed by the user without
needing to write a new dedicated content generator. While this will be much more
difficult to compose a page/booklet, it allows far greater flexibility. 

To help ease the additional complexity that will result from this, a GUI will be
developed allowing the user to dynamically divide up their page and assign 
content items to it. This could be from a drop down menu, or drag and drop.

**Pseudo-Printable Margins**

Future support for printable margins will be added, even on printers which don't
support edge to edge printing. This obviously has some **very** big caveats 
since we're not dealing with magic here. This will be achieved only for pages 
smaller than a sheet.

  * It will initially be limited to a single edge / margin (the inner edge)

  * It won't extend the entire length of that edge. Outer print margins will
    still apply.


**Background Images**

Eventually background image support will be supported for a page. Obviously, 
background images can be supported for an individual element if that elements 
generator supports it.

This is primarily intended for something like a calendar, where a subdued 
background image might be appropriate.


## Generating Booklets

Booklets can be generated by running the ``generate_booklet_collections.py`` 
script. Booklet configs will obviously need to be created prior.

    python3 .\generate_booklet_collections.py --booklet Examples -i ./booklet_collections -o ./_out


## Printing the PDFs

Most printing dialogs will automatically scale a sheet to fit a printable 
margin. Given our desire to have precise alignemnt, the "No scaling" option 
should be chosen prior to printing. Additionally, the correct sheet size and 
orientation should be chosen. 

On Gnome3 in Linux, the page scaling can be selected in the "Page Handling" tab
and should be set to "None".

Notes:

  * There seems to be a bug in the Gnome3 printer dialog when trying to tile
    smaller pages onto a larger sheet with the dialog.

  * Some printers don't handle duplex (double sided) printing well. Content on 
    the front and back of the sheet may be quite mis-aligned. If this bothers 
    you, it you can try printing every other page in single-sided mode, then 
    feed the stack back through the printer to print the other pages.

  * Almost all printers have a margin that is non-printable. This margin will 
    vary based on printer model, and manufacturer. Additionally, the printable 
    margin can be different for different edges on a page.



