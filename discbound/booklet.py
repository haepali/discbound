import os
import copy
import hashlib
import svgwrite

from discbound.unit import Unit
from discbound.page import Page
from discbound.sheet import Sheet
from discbound.papersize import PaperSize
import discbound.layout as layout
import discbound.contents as contents
import discbound.booklets as booklets
import discbound.papersize as papersize


_blank_page_template = {
    "size": "junior",
    "orientation": "portrait",
    "punch_edge": "right",
    "header_height": [0, "in"],
    "footer_height": [0, "in"],
    "margins": "default_imperial",
    "header": None,
    "footer": None,
    "body": None
}


class Booklet():
    def __init__(self, params=None):
        self.sheet_size = params["sheet_size"]
        self.sheet_orientation = params.get("sheet_orientation", "portrait")
        self.layout = params.get("layout", "accordion")
        self.base_path = params["base_path"]
        self.base_name = params.get("base_name", "")
        self.pages = params.get("pages", [])
        self.page_orientation = self.pages[0]["orientation"] if len(self.pages) > 0 else None
        self.page_size = self.pages[0]["size"] if len(self.pages) > 0 else None
        self.pages_per_sheet = 0
        self.collection = params.get("collection", None)

        # Inject Page Numbers
        for i,pg in enumerate(self.pages):
            pg["page_number"] = i+1

        if len(self.pages) == 0:
            return

        # Calculate the number of blank pages needed
        page_size = self.pages[0]["size"]
        page_orientation = self.pages[0]["orientation"]
        sheet_size = self.sheet_size
        sheet_orientation = self.sheet_orientation
        self.imposition = layout.get_imposition(page_size, page_orientation,
                                                sheet_size, sheet_orientation)
        self.pages_per_sheet = self.imposition.pages_per_sheet * 2

        # Add blank pages to make an even sheet
        num_pages = len(self.pages)
        over_pages = num_pages % self.pages_per_sheet
        buffer_pages = 0 if over_pages == 0 else self.pages_per_sheet - over_pages

        # Add blank pages
        last_page_num = self.pages[-1]["page_number"]
        for i in range(1, buffer_pages+1):
            pgnum = last_page_num + i
            buffer_page = copy.deepcopy(_blank_page_template)

            buffer_page["size"] = self.pages[-1]["size"]
            buffer_page["orientation"] = self.pages[-1]["orientation"]
            buffer_page["punch_edge"] = "right" if pgnum % 2 == 1 else "left"
            buffer_page["page_number"] = pgnum
            self.pages.append(
                buffer_page
            )

        self.booklet = self


    def generate(self):
        os.makedirs(self.base_path, exist_ok=True)

        # Generate sheets
        num_pages = len(self.pages)
        sheet_num = 0
        for s_pages in layout.get_sheets(self.layout, self.imposition, num_pages):
            s = Sheet(
                size=self.sheet_size,
                orientation=self.sheet_orientation,
                pages=[self.pages[pg] for pg in s_pages[0]],
                flip=s_pages[1],
                imposition=self.imposition
            )

            # Open file
            file_name = "{}.svg".format(str(sheet_num).zfill(3))
            file_path = os.path.join(self.base_path, file_name)
            fsvg = open(file_path, mode="w")

            # Generate SVG and write out
            s.generate()
            xml_str = s.svg.tostring()
            pretty_xml = svgwrite.utils.pretty_xml(xml_str)
            fsvg.write(pretty_xml)

            # Close file
            fsvg.close()

            sheet_num += 1



class BookletTemplateNamedParams():
    """A BookletTemplateGeneric is used as a template to generate a booklet.

    The group, id, and param_ids are used to select the content generator, as 
    well as pick up prenamed params. The param_ids are then used to create a 
    unique file name.

    This is intended to easily make multiple variations of simple templates. For
    example, creating multiple different grid layouts, or creating the same 
    page with different themes.
    """
    def __init__(self, params):
        self.out_path = params["out_path"]
        self.custom_path_fragment = params["custom_path_fragment"]
        self.sheet_size = params["sheet_size"]
        self.sheet_orientation = params["sheet_orientation"]
        self.page_size = params["page_size"]
        self.page_orientation = params["page_orientation"]
        self.layout = params.get("layout", "accordion")
        self.page_count = params.get("page_count", -1)
        if self.page_count == -1:
            self.page_count = 4 # TODO: properly calculate this
        self.collection = params.get("collection", "default")

        self.theme = None

        # header
        self.header = params.get("header", None)
        self.header_group = params.get("header_group", None)
        self.header_id = params.get("header_id", None)
        self.header_param_ids = params.get("header_param_ids", [])

        # footer
        self.footer = params.get("footer", None)
        self.footer_group = params.get("footer_group", None)
        self.footer_id = params.get("footer_id", None)
        self.footer_param_ids = params.get("footer_param_ids", [])

        # body
        self.body = params.get("body", None)
        self.body_group = params.get("body_group", None)
        self.body_id = params.get("body_id", None)
        self.body_param_ids = params.get("body_param_ids", [])

        # Expand into fully defined pages
        self.pages = []
        self.expand_pages()

        full_booklet_config = {
            "base_path": "",
            "base_name": "",
            "sheet_size": self.sheet_size,
            "sheet_orientation": self.sheet_orientation,
            "layout": self.layout,
            "pages": self.pages
        }

        md5_hex = hashlib.md5(str(full_booklet_config).encode()).hexdigest()

        base_path = os.path.join(
            str(self.out_path),
            str("_".join([
                self.body_id,
                str(self.custom_path_fragment),
                str(self.collection),
                self.layout,
                self.sheet_size,
                self.page_size,
                md5_hex
            ]))
        )

        full_booklet_config["base_path"] = base_path

        self.booklet = Booklet(full_booklet_config)

    def expand_pages(self):
        """Creates the page dictionaries from content groups and param ID's.
        """
        for i in range(1, self.page_count + 1):
            # Base
            p = {
                # Dimensions
                "size": self.page_size,
                "orientation": self.page_orientation,
                "punch_edge": "left" if i % 2 == 1 else "right",
                "header_height": [0, "in"],
                "footer_height": [0, "in"],
                "margins": "default_imperial",

                # Content Areas
                "theme": self.theme,
                "page_number": i,

                "header": None,
                "footer": None,
                "body": None
            }
            # Body
            if self.body_group is not None and self.body_id is not None:
                p["body"] = {
                    "group": self.body_group,
                    "type": self.body_id,
                    "params": contents.get_named_params(
                        self.body_group,
                        self.body_id,
                        self.body_param_ids
                    )
                }
            # Header
            if self.header_group is not None and self.header_id is not None:
                p["header_height"] =  [0.75, "in"]
                p["header"] = {
                    "group": self.header_group,
                    "type": self.header_id,
                    "params": contents.get_named_params(
                        self.header_group,
                        self.header_id,
                        self.header_param_ids
                    )
                }
            # Footer
            if self.footer_group is not None and self.footer_id is not None:
                p["footer_height"] =  [0.75, "in"]
                p["footer"] = {
                    "group": self.footer_group,
                    "type": self.footer_id,
                    "params": contents.get_named_params(
                        self.footer_group,
                        self.footer_id,
                        self.footer_param_ids
                    )
                }
            self.pages.append(p)

    def generate(self):
        self.booklet.generate()


class  BookletTemplateNamed():
    """BookletTemplateNamed is used to select a predefined booklet generator.

    This allows for more complex booklets to be created. For example, a calendar
    booklet can be created with both grid calendars, pages for each week, and
    note pages all mixed together. This also allows passing parameters to the
    generator for high level customization. Continuing with the calendar 
    example, you would want to pass the year, and maybe a theme.
    """
    def __init__(self, params):
        self.out_path = params["out_path"]
        self.sheet_size = params["sheet_size"]
        self.sheet_orientation = params["sheet_orientation"]
        self.page_size = params["page_size"]
        self.page_orientation = params["page_orientation"]
        self.layout = params.get("layout", "accordion")
        self.booklet_config = params["booklet_config"]
        self.collection = params.get("collection", "default")

        self.theme = None

        self.group = self.booklet_config["booklet_group"]
        self.type = self.booklet_config["booklet_type"]
        self.params = self.booklet_config["booklet_params"]

        # Get custom page generator, generate page definitions
        _generator_class = booklets.booklet_factory(self.group, self.type)
        self.page_generator = _generator_class(
            self.page_size,
            self.page_orientation,
            self.params
        )
        self.pages = self.page_generator.generate_pages()
        self.custom_path_fragment = self.page_generator.get_custom_path_fragment()

        full_booklet_config = {
            "base_path": "",
            "base_name": "",
            "sheet_size": self.sheet_size,
            "sheet_orientation": self.sheet_orientation,
            "layout": self.layout,
            "pages": self.pages
        }

        md5_hex = hashlib.md5(str(full_booklet_config).encode()).hexdigest()
        base_path = os.path.join(
            str(self.out_path),
            str("_".join([
                self.type,
                str(self.custom_path_fragment),
                str(self.collection),
                self.layout,
                self.sheet_size,
                self.page_size,
                md5_hex
            ]))
        )

        full_booklet_config["base_path"] = base_path

        self.booklet = Booklet(full_booklet_config)

    def generate(self):
        self.booklet.generate()

