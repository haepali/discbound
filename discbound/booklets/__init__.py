from . import calendars
from . import _testpgnum
from . import _gridsamples

_booklet_maps = {
    "calendars.monthly": calendars.CalendarMonthly,
    "_testpgnum.testpagenumber": _testpgnum.TestPageNumber,
    "_gridsamples.gridsamples": _gridsamples.GridSamples
}

def booklet_factory(booklet_group, booklet_id):
    return _booklet_maps[booklet_group + "." + booklet_id]
