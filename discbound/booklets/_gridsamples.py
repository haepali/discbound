from discbound import booklet
import json

grid_types = ["dotgrid", "grid"]

all_sizes = [
    #[[0.075, "mm"], [0.075, "mm"], [0.075, "mm"]], # 2
    [[0.075, "mm"], [0.1,   "mm"], [0.125, "mm"]], # 3
    #[[0.075, "mm"], [0.125, "mm"], [0.15,  "mm"]], # 4
]

all_spacing = [
    [[1, "mm"], 10],
    [[0.1, "in"], 10]
    #[[0.125, "in"], 8],
    #[[0.0625, "in"], 16]
]

color_groups = {
    "red": [
        #"#450000", # too light
        #"#5c0a0a", # too light
        #"#7e1818", # too dark
        "#9f1919",
        "#e55858",
    ],
    #"red2": [
    #    "#4d0000",
    #    "#650000",
    #    "#9c0000",
    #    "#d80000",
    #    "#ff6c6c",
    #],
    "green": [
        #"#254200", # too light
        "#436b10", 
        "#6ea12f",
        "#a4d467",
        #"#c0df99", # too dark
    ],
    "blue": [
        "#29557a",
        "#4c7598",
        "#5f819c",
        #"#809fb8", # too dark
        #"#a7bcce", # too dark
    ],
    "grey": [

    ],
    "turquoise": [
        #"#0a3e37", # too light
        "#164b44",
        "#285e57",
        "#43766f",
        #"#62908b", # too dark
    ]
}


class GridSamples():
    def __init__(self, page_size, page_orientation, params):
        self.page_size = page_size
        self.page_orientation = page_orientation
        self.base_color = params.get("base_color", "All")
        self.major_color = params.get("major_color", None)

    def generate_pages(self):

        booklet_pages = []
        pg_num = 0
        color_group = []
        if self.base_color is "All":
            for bc in sorted(color_groups.keys()):
                color_group += color_groups[bc]
        else:
            color_group = color_groups[self.base_color]

        for c in color_group:
            for gt in grid_types:
                for si in all_sizes:
                    for sp in all_spacing:
                        pg_num += 1
                        colors = {
                            "minor": c,
                            "semi_major": c,
                            "major": c,
                            "border": "black"
                        }

                        sizes = {
                            "minor": si[0],
                            "semi_major": si[1],
                            "major": si[2],
                            "border": [0.4, "mm"]
                        }

                        spacing = {
                            "pitch": sp[0],
                            "sub_squares": sp[1]
                        }

                        param_string_top = "sizes: " + ", ".join(["{}{}".format(i[0], i[1]) for i in si])
                        param_string_bottom = "colors: {} spacing: {}".format(",".join(c), str(sp[0][0]) + sp[0][1])

                        page = {
                            "size": self.page_size,
                            "orientation": self.page_orientation,
                            "punch_edge": None,
                            "header_height": [0.25, "in"],
                            "footer_height": [0.25, "in"],
                            "margins": None,
                            "theme": None,
                            "page_number": pg_num,
                            "background_color": None, # "red" if pg_num % 2 == 0 else "blue",
                            "header": {
                                "group": "crosses",
                                "type": "cross",
                                "params": {
                                    "cross_alignment": {
                                        "horizontal_overlap_perc": 0.75,
                                        "vertical_overlap_dy": [1.875, "mm"],
                                        "vertical_overlap_origin": "bottom"
                                    },
                                    "cross_style": {
                                        "color": "grey",
                                        "line_width": [0.25, "mm"]
                                    },
                                    "left_text": {"text": param_string_top},
                                    "right_text": {}
                                }
                            },
                            "footer": {
                                "group": "crosses",
                                "type": "cross",
                                "params": {
                                    "cross_alignment": {
                                        "horizontal_overlap_perc": 0.75,
                                        "vertical_overlap_dy": [1.875, "mm"],
                                        "vertical_overlap_origin": "bottom"
                                    },
                                    "cross_style": {
                                        "color": "grey",
                                        "line_width": [0.25, "mm"]
                                    },
                                    "left_text": {"text": param_string_bottom},
                                    "right_text": {"text": str(pg_num)}
                                }
                            },
                            "body": {
                                "group": "grids",
                                "type": gt,
                                "params": {
                                    "colors": colors,
                                    "sizes": sizes,
                                    "spacing": spacing
                                }
                            }
                        }

                        booklet_pages.append(page)

        return booklet_pages

    def get_custom_path_fragment(self):
        return "grid-samples-{}".format(self.base_color)