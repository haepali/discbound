import copy

class TestPageNumber():
    def __init__(self, page_size, page_orientation, params):
        self.page_size = page_size
        self.page_orientation = page_orientation
        self.num_pages = params.get("page_count", 4)

        self._pgnum_template = {
            "size": self.page_size,
            "orientation": self.page_orientation,
            "punch_edge": "right",
            "header_height": [0, "in"],
            "footer_height": [0, "in"],
            "margins": None,
            "theme": None,
            "header": None,
            "footer": None,
            "body": {
                "group": "_testpgnum",
                "type": "testpagenumber",
                "params": {}
            }
        }

    def generate_pages(self):
        out_pages = []
        for i in range(self.num_pages):
            cur_page = copy.deepcopy(self._pgnum_template)
            cur_page["punch_edge"] = "left" if i % 2 == 0 else "right"
            out_pages.append(cur_page)

        return out_pages


    def get_custom_path_fragment(self):
        return "testpgnum_{}".format(self.num_pages)