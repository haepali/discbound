import copy
import calendar
import json

class CalendarMonthly():
    def __init__(self, page_size, page_orientation, params):
        self.page_size = page_size
        self.page_orientation = page_orientation
        self.year = params["year"]
        self.base_color = "skyblue"
        self.dotgrid_color = "skyblue"

        self._blank_template = {
            # Dimensions
            "size": self.page_size,
            "orientation": self.page_orientation,
            "header_height": [0, "in"],
            "footer_height": [0, "in"],
            "margins": "default_imperial",

            # Content Areas
            "theme": "default",
            "header": None,
            "footer": None,
            "body": None
        }

    def _generate_title_page(self):
        return {
            **self._blank_template,
            "header_height": [0.5, "in"],
            "header": {
                "group": "crosses",
                "type": "cross",
                "params": {
                    "cross_alignment": {
                        "horizontal_overlap_perc": 0.75,
                        "vertical_overlap_dy": [1.652, "mm"],
                        "vertical_overlap_origin": "bottom"
                    },
                    "cross_style": {
                        "color": "black",
                        "line_width": [0.2, "mm"],
                        "show_vertical": False
                    },
                    "left_text": {},
                    "right_text": {}
                }
            },
            "footer_height": [0.5, "in"],
            "footer": {
                "group": "crosses",
                "type": "cross",
                "params": {
                    "cross_alignment": {
                        "horizontal_overlap_perc": 0.75,
                        "vertical_overlap_dy": [1.652, "mm"],
                        "vertical_overlap_origin": "top"
                    },
                    "cross_style": {
                        "color": "black",
                        "line_width": [0.2, "mm"],
                        "show_vertical": False
                    },
                    "left_text": {},
                    "right_text": {}
                }
            },
            "body": {
                "group": "titlepages",
                "type": "titlepagelayeredtext",
                "params": {
                    "title_text": {
                        "text": str(self.year),
                        "font_color": "black",
                        "font_family": "Alex Brush",
                        "font_size": [0.6, "in"]
                    }
                }
            }
        }


    def _header_def(self, month, year, side):
        if side == "left":
            return {
                "header_height": [0.5, "in"],
                "header": {
                    "group": "lines",
                    "type": "underline",
                    "params": {
                        "line_color": "black",
                        "line_width": [0.2, "mm"]
                    }
                }
            }

        elif side == "right":
            return {
                "header_height": [0.5, "in"],
                "header": {
                    "group": "crosses",
                    "type": "cross",
                    "params": {
                        "cross_alignment": {
                            "horizontal_overlap_perc": 0.75,
                            "vertical_overlap_dy": [1.652, "mm"],
                            "vertical_overlap_origin": "bottom"
                        },
                        "cross_style": {
                            "color": "black",
                            "line_width": [0.2, "mm"],
                            "show_vertical": False
                        },
                        "left_text": {
                            "text": str(calendar.month_name[month]),
                            "font_family": "Lobster Two",
                            "font_weight": "bold",
                            "font_size": [0.4, "in"]
                        },
                        "right_text": {
                            "text": str(year),
                            "font_family": "Alex Brush",
                            "font_size": [0.4, "in"]
                        }
                    }
                }
            }

    def _cal_body(self, month, year, side):
        if side == "left":
            half = 1
            note_area = {
                "show_note_text": True,
                "show_note_area": False,
                "note_text": {
                    "font_family": "EB Garamond SC",
                    "font_color": "black",
                    "font_size": [0.075, "in"]
                }
            }
        elif side == "right":
            half = 2
            note_area = {
                "show_note_text": False,
                "show_note_area": False,
                "note_text": {}
            }

        return {
            "body": {
                "group": "calendars",
                "type": "calendarmonthsquare",
                "params": {
                    "date": {
                        "month": month,
                        "year": year,
                        "half": half,
                        "first_day_of_week": calendar.SUNDAY
                    },
                    "weekday_bar": {
                        "background_color": "black",
                        "underline": False,
                        "underline_color": "black",
                        "weekday_text": {
                            "font_family": "Arial",
                            "font_color": "white",
                            "font_weight": "bold"
                        }
                    },
                    "body": {
                        "line_color": "black",
                        "background_color": "white",
                        "day_bar_background_color": "black",
                        "weekend_background_color": self.base_color,
                        "weekday_text": {
                            "font_family": "Arial",
                            "font_weight": "bold",
                            "font_color": "white",
                            "font_size": [0.075, "in"]
                        },
                        "date_text": {
                            "font_family": "Arial",
                            "font_weight": "bold",
                            "font_color": "black",
                            "font_size": [0.075, "in"]
                        },
                        "event_text": {
                            "font_family": "Arial",
                            "font_weight": "bold",
                            "font_color": "black",
                            "font_size": [0.075, "in"]
                        },
                        "birthday_text": {
                            "font_family": "Arial",
                            "font_weight": "bold",
                            "font_color": "black",
                            "font_size": [0.075, "in"]
                        }
                    },
                    "note_area": note_area
                }
            }
        }

    def _dotgrid_body(self, month, year, side):
        return {
            "body": {
                "group": "grids",
                "type": "dotgrid",
                "params": {
                    "colors": {
                        "minor": self.dotgrid_color,
                        "semi_major": self.dotgrid_color,
                        "major": self.dotgrid_color,
                        "border": "black"
                    },
                    "sizes": {
                        "minor": [0.25, "mm"],
                        "semi_major": [0.25, "mm"],
                        "major": [0.25, "mm"],
                        "border": [0.4, "mm"]
                    },
                    "spacing": {"pitch": [0.2, "in"], "sub_squares": 5}
                }
            }
        }

    def _generate_pages_month(self, month, year):
        """Generates all pages for a single month.
        
        In a Junior format this consists of:
            * left calendar half
            * right calendar half
            * left dotgrid note area
            * right dotgrid note area
        """

        _cal_left = {
            **self._blank_template,
            **self._header_def(month, year, "left"),
            **self._cal_body(month, year, "left")
        }

        _cal_right = {
            **self._blank_template,
            **self._header_def(month, year, "right"),
            **self._cal_body(month, year, "right")
        }

        _dotgrid_left = {
            **self._blank_template,
            **self._header_def(month, year, "left"),
            **self._dotgrid_body(month, year, "left")
        }

        _dotgrid_right = {
            **self._blank_template,
            **self._header_def(month, year, "right"),
            **self._dotgrid_body(month, year, "right")
        }

        return [
            _cal_left, _cal_right, _dotgrid_left, _dotgrid_right
        ]


    def generate_pages(self):
        """
        """


        out_pages = []

        out_pages.append(self._generate_title_page())
        
        # Pages for each month
        for month in range(1, 13):
            out_pages += self._generate_pages_month(month, self.year)

        # Trailing note page
        out_pages.append(
            {
                **self._blank_template,
                **self._header_def(None, self.year, "left"),
                **self._dotgrid_body(None, self.year, "left")
            }
        )

        # Inject page numbers / punch edges
        for i in range(len(out_pages)):
            pgnum = i + 1
            out_pages[i]["page_number"] = pgnum
            out_pages[i]["punch_edge"] = "right" if pgnum % 2 == 0 else "left"

        return out_pages

    def get_custom_path_fragment(self):
        return str(self.year)

