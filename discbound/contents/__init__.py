import os
import json

from . import grids
from . import calendars
from . import crosses
from . import lines
from . import rectangles
from . import titlepages
from . import _testpgnum

_base_path = os.path.dirname(os.path.realpath(__file__))

# Generator Classes
# -----------------------------------------------------------------------------
_content_maps = {
    "grids.dotgrid": grids.DotGrid,
    "grids.grid": grids.Grid,
    "calendars.calendarmonthsquare": calendars.CalendarMonthSquare,
    "calendars.calendarweek": calendars.CalendarWeek,
    "crosses.cross": crosses.Cross,
    "lines.lines": lines.Lines,
    "lines.underline": lines.Underline,
    "rectangles.rectangle": rectangles.Rectangle,
    "titlepages.titlepagelayeredtext": titlepages.TitlePageLayeredText,
    "_testpgnum.testpagenumber": _testpgnum.TestPageNumber
}

def content_factory(content_group, content_id):
    return _content_maps[content_group + "." + content_id]


# Named Parameter Groups
# -----------------------------------------------------------------------------
_named_param_group_definitions = {
    "grids.dotgrid": "grids.json"
}

# Load the named parameter files
_named_param_groups = {}
for group_id, def_file in _named_param_group_definitions.items():
    with open(os.path.join(_base_path, "named_param_groups", def_file), mode="r") as named_def_f:
        _named_param_groups[group_id] = json.load(named_def_f)

def get_named_params(content_group, content_id, named_params_list):
    cur_named_params = _named_param_groups[content_group + "." + content_id]
    param_group_order = cur_named_params["parameter_group_order"]
    param_groups = cur_named_params["parameter_groups"]

    out_params = {}

    for param_group, named_param in zip(param_group_order, named_params_list):
        out_params[param_group] = param_groups[param_group][named_param]
    
    return out_params