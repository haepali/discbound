import sys
import calendar
import svgwrite

from discbound.element import Element
from discbound.textparams import TextParams
from discbound.unit import Unit

class TestPageNumber(Element):
    def generate(self):

        self.svg.add(
            svgwrite.shapes.Rect(
                insert=(
                    Unit(0, "in").to_svg(),
                    Unit(0, "in").to_svg()
                ),
                size=(
                    self.width.to_svg(),
                    self.height.to_svg()
                ),
                fill="lightgrey"
            )
        )

        x = self.width / 2
        y = self.height / 2
        font_size = y / 2
        tp = TextParams({
            "text": str(self.page_number),
            "font_size": font_size.to_array(),
            "text_anchor": "middle"
        })

        self.svg.add(
            svgwrite.text.Text(
                insert=(
                    x.to_svg(),
                    (y+(font_size/3)).to_svg()
                ),
                **tp.to_svg_dict(),
                fill_opacity=1.0
            )
        )