import sys
import calendar
import svgwrite

from discbound.element import Element
from discbound.textparams import TextParams
from discbound.unit import Unit


def formatdate(year, month, day, date_format=""):
    """Given the year, month and day, return a formatted string representation.

    The `date_format` parameter is a normal Python3 string formatter. If not 
    specified or empty, it defaults to ``'{year} {month} {day}{postfix}'``.
    Allowed field_name's are:

        * ``{year}``
        * ``{year_abbr}``
        * ``{month}``
        * ``{month_abbr}``
        * ``{day}``
        * ``{weekday}``
        * ``{weekday_abbr}``
        * ``{postfix}``

    The term 'day' is a somewhat ambiguous word. It could mean 'day of the
    week' or 'day of the month'. To be clear, 'day' will refer exlcusively to 
    'day of the month'. 'date' will refer to a complete date including 'year',
    'month', and 'day' as it was previously defined. 'weekday' will refer to the
    name of the weekday for the given date as returned by 'day_name' in the
    Python3 calendar module.

    If the specified ``day`` is 0 and the month and year are otherwise valid an
    empty string is returned. If the date is otherwise invalid a ``ValueError``
    exception is raised.
    """

    if day == 0:
        return ""

    # Get the weekday index first. This will serve as check that the provided
    # date is a legal date.
    weekday_idx = calendar.weekday(year, month, day)

    if date_format == "":
        date_format = "{year} {month} {day}{postfix}"

    postfix_list = [
        "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th", 
        "th"  # This is reserved for numbers between 10 and 20 non-inclusive
    ]

    # Set up our format variables
    year_abbr = str((year % 100))
    year = str(year )
    month_abbr = str(calendar.month_abbr[month])
    month = str(calendar.month_name[month])
    weekday_abbr = str(calendar.day_abbr[weekday_idx])
    weekday = str(calendar.day_name[weekday_idx])
    postfix = ""
    if day > 0:
        postfix_idx = day % 10
        if day > 10 and day < 20:
            postfix_idx = 10
        postfix = postfix_list[postfix_idx]

    date_str = date_format.format(
        year=year,
        year_abbr=year_abbr,
        month=month,
        month_abbr=month_abbr,
        day=day,
        weekday=weekday,
        weekday_abbr=weekday_abbr,
        postfix=postfix
    )

    return date_str

def get_month_matrix(year, month, fdow):
    try:
        cur_fdow = calendar.firstweekday()
        calendar.setfirstweekday(fdow)
        days_matrix = calendar.monthcalendar(year, month)
    except Exception:
        pass
    finally:
        calendar.setfirstweekday(cur_fdow)

    return days_matrix

def get_month_slice(year, month, weekdays=None, weeks=None, first_day_of_week=None):
    """Returns a slice of the list of days for the given year and month.

    The slice is based on the provided ``weekdays`` list and ``weeks`` list. 
    This can be visualized as treating the month as a grid with days of the week
    as the columns and weeks of the month as the rows. ``weekdays`` specifies 
    the columns to use, and ``weeks`` specifies the rows. The return list will
    be padded with 0's for dates that are out of range for the given month 
    and year.

    ``weekdays`` is a list of days of the week, with valid values being in the 
    range [0, 6]. 

    ``weeks`` is a list of week indices for the month. Valid indices are in the
    range [0, 5].
    """

    if weekdays is None:
        weekdays = set(range(7))
    if weeks is None:
        weeks = set(range(6))

    # Convert to sets to remove duplicates
    weekdays = set(weekdays)
    weeks = set(weeks)

    if first_day_of_week is None:
        first_day_of_week = calendar.firstweekday()
    days_matrix = get_month_matrix(year, month, first_day_of_week)

    num_pad_weeks = 6 - len(days_matrix)
    days_matrix += num_pad_weeks * [[0, 0, 0, 0, 0, 0, 0]]

    out_days = []

    for cur_week in range(6):
        for cur_day_of_week in range(7):
            if cur_week in weeks and cur_day_of_week in weekdays:
                out_days.append(days_matrix[cur_week][cur_day_of_week])

    return out_days


class DateParams():

    def __init__(self, params):
        self.month = int(params["month"])
        self.year = int(params["year"])
        self.week = int(params.get("week", -1))
        if self.week == -1:
            self.weeks = list(range(0, 7))
        else:
            self.weeks = [self.week]
        self.num_days = params.get("num_days", 7)
        self.day_start = params.get("day_start", 0)
        self.day_end = self.day_start + self.num_days
        self.first_day_of_week = params.get("first_day_of_week", calendar.MONDAY)

        if "half" in params:
            half = params["half"]
            day_ranges = [
                (0, 7),  # 0: whole week
                (0, 4),  # 1: first half (4 days)
                (4, 7),  # 2: second half (3 days)
                (0, 3),  # 3: first half (3 days)
                (3, 7)   # 4: second half (4 days)
            ]
            self.day_start, self.day_end = day_ranges[half]
            self.num_days = self.day_end - self.day_start

        self.spacer = self.num_days == 3


class CalendarNoteAreaParams():

    def __init__(self, params):
        self.note_text = TextParams(params["note_text"])
        self.show_note_text = params["show_note_text"]
        self.show_note_text = params["show_note_text"]
        self.show_note_area = params.get("show_note_area", True)


class CalendarWeekdayBarParams():

    def __init__(self, params):
        self.background_color = params["background_color"]
        self.underline = params["underline"]
        self.underline_color = params["underline_color"]
        self.weekday_text = TextParams(params["weekday_text"])


class CalendarBodyParams():

    def __init__(self, params):
        self.line_color = params.get("line_color", "black")
        self.line_width = Unit(*params.get("line_width", [0.05, "mm"]))
        self.background_color = params.get("background_color", "white")
        self.day_bar_background_color = params.get("day_bar_background_color", "black")
        self.weekend_background_color = params.get("weekend_background_color", "skyblue")
        self.date_text = TextParams(params["date_text"])
        self.event_text = TextParams(params["event_text"])
        self.birthday_text = TextParams(params["birthday_text"])


class CalendarParams():

    def __init__(self, params):
        self.date = DateParams(params["date"])
        self.weekday_bar = CalendarWeekdayBarParams(params.get("weekday_bar", {}))
        self.body = CalendarBodyParams(params["body"])
        self.note_area = CalendarNoteAreaParams(params["note_area"])


class CalendarMonthSquare(Element):

    def _load_params(self):
        self.params = CalendarParams(self.raw_params)

    def _generate_weekday_cell(self, x, y, width, height, weekday):

        mysvg = svgwrite.container.SVG(
            insert=(x.to_svg(), y.to_svg()),
            size=(width.to_svg(), height.to_svg())
        )

        text_border = Unit(1, "mm")
        weekday_font_size = height - text_border

        mysvg.add(  # Weekday color bar
            svgwrite.shapes.Rect(
                insert=(
                    (Unit(0, "in")).to_svg(),
                    (Unit(0, "in")).to_svg()
                ),
                size=(
                    width.to_svg(),
                    height.to_svg()
                ),
                fill=self.params.weekday_bar.background_color
            )
        )
        mysvg.add(  # Weekday text
            svgwrite.text.Text(
                text=weekday,
                insert=(
                    text_border.to_svg(),
                    weekday_font_size.to_svg()
                ),
                fill=self.params.weekday_bar.weekday_text.font_color,
                font_weight=self.params.weekday_bar.weekday_text.font_weight,
                font_size=weekday_font_size.to_svg(),
                font_family=self.params.weekday_bar.weekday_text.font_family
            )
        )

        return mysvg

    def _generate_weekday_bar(self, x, y, width, height):

        date = self.params.date
        num_days = date.num_days + 1 if date.spacer else date.num_days
        day_width = width / num_days
        day_start = date.day_start
        day_end = date.day_start + num_days
    
        mysvg = svgwrite.container.SVG(
            insert=(x.to_svg(), y.to_svg()),
            size=(width.to_svg(), height.to_svg())
        )

        # Add weekday name bar

        # Get days of week list
        dow = list(calendar.day_name)
        # Rotate by our desired first day of the week
        dow = dow[date.first_day_of_week:] + dow[:date.first_day_of_week]
        if date.spacer:
            dow += ["Highlights"]

        for i,d in enumerate(range(day_start, day_end)):
            mysvg.add(
                self._generate_weekday_cell(
                    x=(day_width * i),
                    y=Unit(0, "in"),
                    width=day_width,
                    height=height,
                    weekday=dow[d]
                )
            )

        return mysvg

    def _generate_calendar_cell(self, x, y, width, height, day, borders=None):
        if borders is None:
            borders = [1, 1, 1, 1] # Default to printing all borders


        date = self.params.date
        date_text = self.params.body.date_text

        mysvg = svgwrite.container.SVG(
            id="calendar_cell_{year}-{month}-{day}".format(
                year=date.year,
                month=date.month,
                day=day
            ),
            insert=(x.to_svg(), y.to_svg()),
            size=(width.to_svg(), height.to_svg())
        )

        # Outside month day range
        if day == 0: 
            background_color = "lightgrey"
            opacity = 0.2
        # Weekend
        elif calendar.weekday(date.year, date.month, day) in [5, 6]:
            background_color = self.params.body.weekend_background_color
            opacity = 0.2
        # Normal background
        else:
            background_color = "white"
            opacity = 0

        # Background color
        mysvg.add(
            svgwrite.shapes.Rect(
                insert=(Unit(0, "in").to_svg(), Unit(0, "in").to_svg()),
                size=(width.to_svg(), height.to_svg()),
                fill=background_color,
                fill_opacity=opacity
            )
        )

        # Day Text
        date_str = str(day) if day > 0 else ""
        mysvg.add(
            svgwrite.text.Text(
                text=date_str,
                insert=(
                    (Unit(1, "mm")).to_svg(),
                    (date_text.font_size + Unit(1, "mm")).to_svg()
                ),
                font_size=date_text.font_size.to_svg(),
                font_family=date_text.font_family
            )
        )

        # Draw Borders
        for i, side in enumerate(borders):

            x  = int(((i+1) % 4) > 1) # [0, 1, 1, 0, ...]
            y  = int(((i+0) % 4) > 1) # [0, 0, 1, 1, ...]
            xn = int(((i+2) % 4) > 1)
            yn = int(((i+1) % 4) > 1)

            if side:
                #print("{}: ({}, {}) -> ({}, {})".format(i, x, y, xn, yn))
                mysvg.add(
                    svgwrite.shapes.Line(
                        start=(
                            (width * x).to_svg(), # x
                            (height * y).to_svg()  # y
                        ),
                        end=(
                            (width * xn).to_svg(), # x
                            (height * yn).to_svg()  # y
                        ),
                        stroke=self.params.body.line_color,
                        stroke_width=self.params.body.line_width.to_svg()
                    )
                )

        return mysvg

    def _generate_calendar_body(self, x, y, width, height):

        # Local SVG container to make all operations local
        mysvg = svgwrite.container.SVG(
            insert=(x.to_svg(), y.to_svg()),
            size=(width.to_svg(), height.to_svg())
        )

        body = self.params.body
        date = self.params.date

        days = get_month_slice(
            year=date.year,
            month=date.month,
            weekdays=range(date.day_start, date.day_end),
            weeks=date.weeks,
            first_day_of_week=date.first_day_of_week
        )

        # Calculate cell dimensions
        num_weekdays = date.day_end - date.day_start
        num_cols = num_weekdays
        cell_width = width / (num_cols + 1) if date.spacer else width / num_cols
        cell_height = height / 5

        # Generate each cell
        for i,d in enumerate(days):
            if i // num_cols == 5 and d == 0:
                continue

            # Calculate cell dimensions and location
            x = (cell_width * (i % num_cols))
            y = (cell_height * (i // num_cols))
            cur_cell_width = cell_width
            cur_cell_height = cell_height

            # Check whether there is a valid cell in the following week
            iwk = i + num_cols
            has_day_next_week = iwk < len(days) and (days[iwk] > 0 or iwk // num_cols < 5)

            # Check if current cell will need to be squashed
            squashed = has_day_next_week and i // num_cols == 4
            if squashed:
                cur_cell_height = cur_cell_height / 2

            # If we go past 5 weeks, we want to squash the extra days into the 
            # last week.
            if i // num_cols == 5:
                y = (cur_cell_height * 4.5)
                cur_cell_height = cur_cell_height / 2
                #print("Week overrun {} {} {}".format(date.year, date.month, d))

            mysvg.add(
                self._generate_calendar_cell(
                    x=x, y=y, width=cur_cell_width, height=cur_cell_height, 
                    day=d,
                    borders=[1, 1, 1, 1]
                    # leaving commented for potential future use
                    #    i // num_cols > 0,
                    #    i % num_cols < num_cols-1,
                    #    has_day_next_week,
                    #    i % num_cols > 0
                    #]
                )
            )

        return mysvg

    def _generate_note_area(self, x, y, width, height):

        mysvg = svgwrite.container.SVG(
            insert=(x.to_svg(), y.to_svg()),
            size=(width.to_svg(), height.to_svg())
        )

        font_size = Unit(0.2, "in")

        # Text
        if self.params.note_area.show_note_text:
            mysvg.add(
                svgwrite.text.Text(
                    text="Notes",
                    insert=(Unit(0, "in").to_svg(), font_size.to_svg()),
                    font_size=font_size.to_svg(),
                    font_family="Alex Brush",
                    font_weight="normal"
                )
            )

        # Line
        mysvg.add(
            svgwrite.shapes.Line(
                start=(
                    Unit(0, "in").to_svg(),
                    (font_size + Unit(0.889, "mm")).to_svg()
                ),
                end=(
                    (width).to_svg(),
                    (font_size + Unit(0.889, "mm")).to_svg()
                ),
                stroke=self.params.body.line_color,
                stroke_width=Unit(0.25, "mm").to_svg()
            )
        )
        
        return mysvg

    def generate(self):
        # month_height
        month_height = Unit(0.5, "in")
        # weekday_height
        month_weekday_spacer = Unit(1, "mm")
        weekday_dy = month_weekday_spacer
        weekday_height = Unit(0.15, "in")
        # calendar_height
        weekday_cal_spacer = Unit(1, "mm")
        calendar_dy = weekday_dy + weekday_cal_spacer + weekday_height
        if self.params.note_area.show_note_area:
            calendar_height = ((self.width / 4) * 5)
        else:
            calendar_height = self.height - calendar_dy

        # note_height
        calendar_note_spacer = Unit(1, "mm")
        note_dy = calendar_dy + calendar_note_spacer + calendar_height
        note_height = self.height - note_dy

        self.svg.add(
            self._generate_weekday_bar(
                Unit(0, "in"),
                weekday_dy,
                self.width, weekday_height
            )
        )
        self.svg.add(
            self._generate_calendar_body(
                Unit(0, "in"),
                calendar_dy,
                self.width, calendar_height
            )
        )
        if self.params.note_area.show_note_area:
            self.svg.add(
                self._generate_note_area(
                    Unit(0, "in"),
                    note_dy,
                    self.width, note_height
                )
            )

        # Outer border
        border_stroke = self.params.body.line_width
        border_width = self.width - border_stroke
        border_height = calendar_height - border_stroke
        border_dx = border_stroke / 2
        border_dy = calendar_dy + (border_stroke / 2)
        self.svg.add(
            svgwrite.shapes.Rect(
                insert=(border_dx.to_svg(), border_dy.to_svg()), 
                size=(border_width.to_svg(), border_height.to_svg()),
                stroke=self.params.body.line_color,
                stroke_width=border_stroke.to_svg(),
                fill="none"
            )
        )




class CalendarWeekParams():
    def __init__(self, params):
        self.date = DateParams(params["date"])


class CalendarWeek(Element):
    def _load_params(self):
        self.params = CalendarWeekParams(self.raw_params)

    def _generate_day_bar(self, x, y, width, height, text=""):

        mysvg = svgwrite.container.SVG(
            insert=(x.to_svg(), y.to_svg()),
            size=(width.to_svg(), height.to_svg())
        )

        font_size = Unit(0.2, "in")

        # Text
        mysvg.add(
            svgwrite.text.Text(
                text=text,
                insert=(Unit(0, "in").to_svg(), font_size.to_svg()),
                font_size=font_size.to_svg(),
                font_family="EB Garamond SC",
                font_weight="normal"
            )
        )

        # Line
        mysvg.add(
            svgwrite.shapes.Line(
                start=(
                    Unit(0, "in").to_svg(),
                    (font_size + Unit(0.889, "mm")).to_svg()
                ),
                end=(
                    (width).to_svg(),
                    (font_size + Unit(0.889, "mm")).to_svg()
                ),
                stroke="grey",
                stroke_width=Unit(0.25, "mm").to_svg()
            )
        )
        
        return mysvg

    def generate(self):
        date = self.params.date

        days = get_month_slice(
            year=date.year,
            month=date.month,
            weekdays=range(date.day_start, date.day_end),
            weeks=[date.week],
            first_day_of_week=date.first_day_of_week
        )

        weekday_height = self.height / 3  #len(days)

        for i,day in enumerate(days):
            if i > 2:  # Want to combine weekends together
                continue

            text = formatdate(date.year, date.month, day, "{month} ~ {weekday} {day}{postfix}")
            if i == 2 and len(days) == 4:
                text += " | " + formatdate(date.year, date.month, days[i+1], "{month} ~ {weekday} {day}{postfix}")

            # Outside month day range
            if day == 0: 
                background_color = "lightgrey"
                opacity = 0.2
            # Weekend
            elif calendar.weekday(date.year, date.month, day) in [5, 6]:
                background_color = "paleturquoise"
                opacity = 0.2
            # Normal background
            else:
                background_color = "white"
                opacity = 0

            # Add background color
            self.svg.add(
                svgwrite.shapes.Rect(
                    insert=(
                        Unit(0, "in").to_svg(),
                        (weekday_height*(i%3)).to_svg()
                    ),
                    size=(
                        self.width.to_svg(),
                        weekday_height.to_svg()
                    ),
                    fill=background_color,
                    fill_opacity=opacity
                )
            )

            # Add text bar
            self.svg.add(
                self._generate_day_bar(
                    x=Unit(0, "in"),
                    y=weekday_height*(i%3),
                    width=self.width,
                    height=weekday_height,
                    text=text
                )
            )
