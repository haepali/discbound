import svgwrite

from discbound.element import Element
from discbound.textparams import TextParams
from discbound.unit import Unit


class CrossAlignment():
    def __init__(self, params, width, height):
        # Read in percentages / set defaults
        vertical_overlap_perc = params.get("vertical_overlap_perc", 0.0)
        vertical_overlap_origin = params.get("vertical_overlap_origin", "top")
        horizontal_overlap_perc = params.get("horizontal_overlap_perc", 0.0)
        horizontal_overlap_origin = params.get("horizontal_overlap_origin", "left")

        # Set Vertical overlap
        try:
            self.vertical_overlap = Unit(*params["vertical_overlap_dy"])
        except:
            self.vertical_overlap = height * vertical_overlap_perc
        
        if vertical_overlap_origin == "bottom":
            self.vertical_overlap = height - self.vertical_overlap

        # Set Horizontal overlap
        try:
            self.horizontal_overlap = Unit(*params["horizontal_overlap_dx"])
        except:
            self.horizontal_overlap = width* horizontal_overlap_perc

        if horizontal_overlap_origin == "right":
            self.horizontal_overlap = width - self.horizontal_overlap


class CrossStyle():
    def __init__(self, params):
        self.color = params.get("color", "grey")
        self.line_width = Unit(*params.get("line_width", [0.25, "mm"]))
        self.show_vertical = params.get("show_vertical", True)
        self.show_horizontal = params.get("show_horizontal", True)


class CrossParams():
    def __init__(self, params, width=Unit(0, "in"), height=Unit(0, "in")):
        self.cross_alignment = CrossAlignment(params["cross_alignment"], width, height)
        self.cross_style = CrossStyle(params.get("cross_style", {}))
        self.left_text = TextParams(params.get("left_text", {}))
        self.right_text = TextParams(params.get("right_text", {}))
        self.text_spacer = Unit(1, "mm")

        # Inject text anchors
        self.left_text.text_anchor = "end"
        self.right_text.text_anchor = "start"


class Cross(Element):
    def _load_params(self):
        self.params = CrossParams(self.raw_params, self.width, self.height)

    def _generate_cross(self):
        align = self.params.cross_alignment
        style = self.params.cross_style

        dx = align.horizontal_overlap
        dy = align.vertical_overlap

        line_segments = {
            "vertical": {"start": (dx, Unit(0, "in")), "end": (dx, self.height)},
            "horizontal": {"start": (Unit(0, "in"), dy), "end": (self.width, dy)}
        }

        if not style.show_horizontal:
            line_segments.pop("horizontal")
        if not style.show_vertical:
            line_segments.pop("vertical")

        for segment in line_segments.values():
            self.svg.add(
                svgwrite.shapes.Line(
                    start=[i.to_svg() for i in segment["start"]],
                    end=[i.to_svg() for i in segment["end"]],
                    stroke=style.color,
                    stroke_width=style.line_width.to_svg()
                )
            )

    def _generate_text(self):

        dx = self.params.cross_alignment.horizontal_overlap

        # Add Left Text
        self.svg.add(
            svgwrite.text.Text(
                insert=(
                    (dx - self.params.text_spacer).to_svg(),
                    (self.params.left_text.font_size).to_svg()
                ),
                **self.params.left_text.to_svg_dict()
            )
        )

        # Add Right Text
        self.svg.add(
            svgwrite.text.Text(
                insert=(
                    (dx + self.params.text_spacer).to_svg(),
                    (self.params.right_text.font_size).to_svg()
                ),
                **self.params.right_text.to_svg_dict()
            )
        )

    def generate(self):
        self._generate_cross()
        self._generate_text()


