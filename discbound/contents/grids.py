import svgwrite

from discbound.element import Element
from discbound.unit import Unit

class GridColors():
    def __init__(self, params):
        self.minor = params["minor"]
        self.semi_major = params["semi_major"]
        self.major = params["major"]
        self.border = params["border"]


class GridSizes():
    def __init__(self, params):
        self.minor = Unit(*params["minor"])
        self.semi_major = Unit(*params["semi_major"])
        self.major = Unit(*params["major"])
        self.border = Unit(*params["border"])


class GridSpacing():
    def __init__(self, params, width, height):
        self.pitch = Unit(*params["pitch"]) # "Minor" unit for the grid
        self.sub_squares = int(params["sub_squares"]) # max number of squares per major grid unit
        self.base_width = self.pitch * self.sub_squares

        # Calculate pitch and dot counts
        self.n_minor = 1
        self.n_semi_major = None
        self.n_major = self.sub_squares
        if self.n_major % 2 == 0:
            self.n_semi_major = int(self.n_major / 2)

        # Calculate the number of dots in our usable area
        if self.pitch.value == 0:
            self.num_cols = 0
            self.num_rows = 0
        else:
            self.num_cols = int((width / self.pitch).value)
            self.num_rows = int((height / self.pitch).value)


class GridParams():
    def __init__(self, params, width=Unit(0, "in"), height=Unit(0, "in")):
        self.colors = GridColors(params["colors"])
        self.sizes = GridSizes(params["sizes"])
        self.spacing = GridSpacing(params["spacing"], width, height)

class Grid(Element):
    def _load_params(self):
        self.params = GridParams(self.raw_params, self.width, self.height)

    def _generate_grid_line(self, idx, orientation):

        # Set up some basic params
        zero_svg = Unit(0, "in").to_svg()
        spacing = self.params.spacing
        sizes = self.params.sizes
        colors = self.params.colors

        # Calculate color and line thickness
        offset_svg = (spacing.pitch * idx).to_svg()
        size = sizes.minor
        color = colors.minor
        if idx % spacing.n_semi_major == 0:
            size = sizes.semi_major
            color = colors.semi_major
        if idx % spacing.n_major == 0:
            size = sizes.major
            color = colors.major

        # Calculate start and end points
        if orientation == "row":
            start = (zero_svg, offset_svg)
            end = (self.width.to_svg(), offset_svg)
        elif orientation == "col":
            start = (offset_svg, zero_svg)
            end = (offset_svg, self.height.to_svg())

        # Add line to parent SVG container
        self.svg.add(
            svgwrite.shapes.Line(
                start=start, end=end, stroke=color, stroke_width=size.to_svg()
            )
        )

    def generate(self):

        for r in range(self.params.spacing.num_rows + 1):
            self._generate_grid_line(r, "row")
        for c in range(self.params.spacing.num_cols + 1):
            self._generate_grid_line(c, "col")



class DotGrid(Element):
    def _load_params(self):
        self.params = GridParams(self.raw_params, self.width, self.height)

    def _generate_major_minor_grid_pattern(self):

        """
            +------------+
            |        |   |
            |    1   |   |
            |        | 2 |
            |--------|   |
            |   3    |   |
            +------------+
        """

        grid_base_size = self.params.spacing.base_width
        grid_pitch = self.params.spacing.pitch
        minor_area_width = grid_pitch * (self.params.spacing.sub_squares - 1)

        grid_area_pattern = svgwrite.pattern.Pattern(
            id=self.new_unique_id("gridArea"),
            insert=(Unit(0, "in").to_svg(), Unit(0, "in").to_svg()),
            size=(grid_base_size.to_svg(), grid_base_size.to_svg()),
            patternUnits="userSpaceOnUse"
        )

        # Minor Area
        grid_area_pattern.add(
            svgwrite.shapes.Rect(
                insert=(Unit(0, "in").to_svg(), Unit(0, "in").to_svg()),
                size=(minor_area_width.to_svg(), minor_area_width.to_svg()),
                fill="url(#{})".format(self.get_unique_id("minorGridDot"))
            )
        )
        # Major Column Area
        grid_area_pattern.add(
            svgwrite.shapes.Rect(
                insert=(minor_area_width.to_svg(), Unit(0, "in").to_svg()),
                size=(grid_pitch.to_svg(), grid_base_size.to_svg()),
                fill="url(#{})".format(self.get_unique_id("majorGridDot"))
            )
        )
        # Major Row Area
        grid_area_pattern.add(
            svgwrite.shapes.Rect(
                insert=(Unit(0, "in").to_svg(), minor_area_width.to_svg()),
                size=(minor_area_width.to_svg(), grid_pitch.to_svg()),
                fill="url(#{})".format(self.get_unique_id("majorGridDot"))
            )
        )

        return grid_area_pattern


    def _generate_semimajor_major_minor_grid_pattern(self):

        """
            +--------+---+--------+---+
            |        |   |        |   |
            |    1   |   |    3   |   |
            |        |   |        |   |
            |--------|   |--------|   |
            |   5    | 2 |    6   |   |
            |--------|   |--------| 4 |
            |        |   |        |   |
            |    7   |   |    8   |   |
            |        |   |        |   |
            |---------------------|   |
            |          9          |   |
            +-------------------------+
        """
        grid_base_size = self.params.spacing.base_width
        grid_pitch = self.params.spacing.pitch
        minor_area_width = grid_pitch * ((self.params.spacing.sub_squares - 2) / 2)
        minor_area_offset = minor_area_width + grid_pitch
        semimajor_col_height = grid_pitch * (self.params.spacing.sub_squares - 1)
        semimajor_row_width = minor_area_width
        major_col_height = grid_base_size
        major_row_width = grid_pitch * (self.params.spacing.sub_squares - 1)

        grid_area_pattern = svgwrite.pattern.Pattern(
            id=self.new_unique_id("gridArea"),
            insert=(Unit(0, "in").to_svg(), Unit(0, "in").to_svg()),
            size=(grid_base_size.to_svg(), grid_base_size.to_svg()),
            patternUnits="userSpaceOnUse"
        )

        grid_areas = [
            { # 1 - Minor
                "insert": (Unit(0, "in").to_svg(), Unit(0, "in").to_svg()),
                "size": (minor_area_width.to_svg(), minor_area_width.to_svg()),
                "fill": "url(#{})".format(self.get_unique_id("minorGridDot"))
            },
            { # 2 - Semi-major
                "insert": (minor_area_width.to_svg(), Unit(0, "in").to_svg()),
                "size": (grid_pitch.to_svg(), semimajor_col_height.to_svg()),
                "fill": "url(#{})".format(self.get_unique_id("semiMajorGridDot"))
            },
            { # 3 - Minor
                "insert": (minor_area_offset.to_svg(), Unit(0, "in").to_svg()),
                "size": (minor_area_width.to_svg(), minor_area_width.to_svg()),
                "fill": "url(#{})".format(self.get_unique_id("minorGridDot"))
            },
            { # 4 - Major
                "insert": (major_row_width.to_svg(), Unit(0, "in").to_svg()),
                "size": (grid_pitch.to_svg(), major_col_height.to_svg()),
                "fill": "url(#{})".format(self.get_unique_id("majorGridDot"))
            },
            { # 5 - Semi-major
                "insert": (Unit(0, "in").to_svg(), minor_area_width.to_svg()),
                "size": (semimajor_row_width.to_svg(), grid_pitch.to_svg()),
                "fill": "url(#{})".format(self.get_unique_id("semiMajorGridDot"))
            },
            { # 6 - Semi-major
                "insert": (minor_area_offset.to_svg(), minor_area_width.to_svg()),
                "size": (semimajor_row_width.to_svg(), grid_pitch.to_svg()),
                "fill": "url(#{})".format(self.get_unique_id("semiMajorGridDot"))
            },
            { # 7 - Minor
                "insert": (Unit(0, "in").to_svg(), minor_area_offset.to_svg()),
                "size": (minor_area_width.to_svg(), minor_area_width.to_svg()),
                "fill": "url(#{})".format(self.get_unique_id("minorGridDot"))
            },
            { # 8 - Minor
                "insert": (minor_area_offset.to_svg(), minor_area_offset.to_svg()),
                "size": (minor_area_width.to_svg(), minor_area_width.to_svg()),
                "fill": "url(#{})".format(self.get_unique_id("minorGridDot"))
            },
            { # 9 - Major
                "insert": (Unit(0, "in").to_svg(), major_row_width.to_svg()),
                "size": (major_row_width.to_svg(), grid_pitch.to_svg()),
                "fill": "url(#{})".format(self.get_unique_id("majorGridDot"))
            }
        ]

        for area in grid_areas:
            grid_area_pattern.add(
                svgwrite.shapes.Rect(**area)
            )

        return grid_area_pattern


    def generate(self):
        defs = svgwrite.container.Defs()
        # Add Minor, Semi-Major, and Major dot patterns
        grid_base_size = self.params.spacing.base_width
        grid_pitch = self.params.spacing.pitch
        dot_patterns = {
            "minorGridDot": {
                "radius": self.params.sizes.minor.to_svg(),
                "color": self.params.colors.minor
            },
            "semiMajorGridDot": {
                "radius": self.params.sizes.semi_major.to_svg(),
                "color": self.params.colors.semi_major
            },
            "majorGridDot": {
                "radius": self.params.sizes.major.to_svg(),
                "color": self.params.colors.major
            }
        }
        for dp, dp_def in dot_patterns.items():
            cur_pattern = svgwrite.pattern.Pattern(
                id=self.new_unique_id(dp),
                insert=(Unit(0, "in").to_svg(), Unit(0, "in").to_svg()),
                size=(grid_pitch.to_svg(), grid_pitch.to_svg()),
                patternUnits="userSpaceOnUse"
            )
            cur_pattern.add(
                svgwrite.shapes.Circle(
                    center=((grid_pitch/2).to_svg(), (grid_pitch/2).to_svg()),
                    r=dp_def["radius"],
                    fill=dp_def["color"]
                )
            )
            defs.add(cur_pattern)

        if self.params.spacing.n_semi_major is not None:
            defs.add(self._generate_semimajor_major_minor_grid_pattern())
        else:
            defs.add(self._generate_major_minor_grid_pattern())

        self.svg.add(defs)
        self.svg.add(
            svgwrite.shapes.Rect(
                insert=(Unit(0, "in").to_svg(), Unit(0, "in").to_svg()),
                size=(self.width.to_svg(), self.height.to_svg()),
                fill="url(#{})".format(self.get_unique_id("gridArea"))
            )
        )
