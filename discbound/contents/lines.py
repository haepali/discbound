import sys
import svgwrite

from discbound.element import Element
from discbound.textparams import TextParams
from discbound.unit import Unit

class LineParams():
    def __init__(self, params, width=Unit(0, "in"), height=Unit(0, "in")):
        self.orientation = params.get("orientation", "horizontal")
        if self.orientation == "vertical":
            temp = width
            width = height
            heigh = temp

        self.anchor = params.get("anchor", "top" if self.orientation == "horizontal" else "left")
        self.line_width = Unit(*params.get("line_width", [0, "in"]))

        self.line_length = width

        self.line_color = params.get("line_color", "grey")
        self.line_pitch = Unit(*params.get("line_pitch", [0, "in"]))
        self.offset = Unit(*params.get("offset", [0, "in"]))
        self.num_lines = 1 # Default number of lines
        self.max_lines = int(params.get("max_lines", -1))
        if self.max_lines < 0:
            self.max_lines = sys.maxsize

        # Calculate the theoretical max number of lines
        if self.line_pitch.value > 0:
            self.num_lines = int(((height - self.offset) / line_pitch).value)

        # Calculate the final line count
        self.num_lines = min([self.num_lines, self.max_lines])


class Lines(Element):
    def _load_params(self):
        self.params = LineParams(self.raw_params, self.width, self.height)

    def generate(self):
        # Set up params for creating orientation aware lines
        if self.params.orientation == "horizontal":
            bx = Unit(0, "in")
            if self.anchor == "bottom":
                pitch = -self.params.pitch
                by = self.height - self.params.offset
            else:
                by = self.params.offset
            dx = self.params.line_length
            dy = Unit(0, "in")
            pitchx = Unit(0, "in")
            pitchy = pitch
        else:
            by = Unit(0, "in")
            if self.anchor == "right":
                pitch = -self.params.pitch
                bx = self.width - self.params.offset
            else:
                bx = self.params.offset
            dx = Unit(0, "in")
            dy = self.params.line_length
            pitchx = pitch
            pitchy = Unit(0, "in")

        # Generate lines
        for i in range(0, self.params.num_lines+1):
            sx = bx + (pitchx * i)
            ex = sx + dx
            sy = by + (pitchy * i)
            ey = sy + dy
            self.svg.add(
                svgwrite.shapes.Line(
                    start=(sx.to_svg(), sy.to_svg()),
                    end=(ex.to_svg(), ey.to_svg()),
                    stroke=self.params.line_color,
                    stroke_width=self.params.line_width.to_svg()
                )
            )


class UnderlineParams():
    def __init__(self, params):
        self.line_width = Unit(*params.get("line_width", [0.25, "mm"]))
        self.line_color = params.get("line_color", "grey")
        self.offset = Unit(*params.get("offset", [1.651, "mm"]))
        self.text = TextParams(params.get("text", {}))
        self.text_anchor = params.get("text_anchor", "left")
        self.text_spacer = Unit(*params.get("text_spacer", [1, "mm"]))


class Underline(Element):
    def _load_params(self):
        self.params = UnderlineParams(self.raw_params)

    def generate(self):
        line_dy = (self.height - self.params.offset)
        self.svg.add(
            svgwrite.shapes.Line(
                start=(
                    Unit(0, "in").to_svg(),
                    line_dy.to_svg()
                ),
                end=(
                    self.width.to_svg(),
                    line_dy.to_svg()
                ),
                stroke=self.params.line_color,
                stroke_width=self.params.line_width.to_svg()
            )
        )

        text_params = self.params.text
        text_anchor = self.params.text_anchor
        text_spacer = self.params.text_spacer
        text_x = Unit(0, "in")
        text_y = line_dy - text_spacer

        if text_anchor == "left":
            text_params.text_anchor = "start"
            text_x = Unit(0, "in")
        elif text_anchor == "center":
            text_params.text_anchor = "middle"
            text_x = self.width / 2
        elif text_anchor == "right":
            text_params.text_anchor = "end"
            text_x = self.width

        self.svg.add(
            svgwrite.text.Text(
                insert=(text_x.to_svg(), text_y.to_svg()),
                **text_params.to_svg_dict()
            )
        )
