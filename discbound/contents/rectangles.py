import sys
import svgwrite

from discbound.element import Element
from discbound.textparams import TextParams
from discbound.unit import Unit

class RectangleParams():
    def __init__(self, params):
        self.color = params.get("color", "white")


class Rectangle(Element):
    def _load_params(self):
        self.params = RectangleParams(self.raw_params)

    def generate(self):
        self.svg.add(
            svgwrite.shapes.Rect(
                insert=(
                    Unit(0, "in").to_svg(),
                    Unit(0, "in").to_svg()
                ),
                size=(
                    self.width.to_svg(),
                    self.height.to_svg()
                ),
                fill=self.params.color
            )
        )


