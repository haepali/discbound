import svgwrite

from discbound.element import Element
from discbound.textparams import TextParams
from discbound.unit import Unit

class TitlePageLayeredText(Element):
    def _load_params(self):
        self.foreground_text = TextParams(self.raw_params["title_text"])
        self.background_text = TextParams(self.raw_params["title_text"])

        # Set size
        self.background_text.font_size = self.foreground_text.font_size / 0.4

        # Override anchors
        self.foreground_text.text_anchor = "middle"
        self.background_text.text_anchor = "middle"

    def generate(self):

        x = self.width / 2
        y = self.height / 2
        background_dy = self.background_text.font_size / 4
        foreground_dy = self.foreground_text.font_size / 4

        # Insert background text
        self.svg.add(
            svgwrite.text.Text(
                insert=(
                    x.to_svg(),
                    (y + background_dy).to_svg()
                ),
                **self.background_text.to_svg_dict(),
                fill_opacity=0.1
            )
        )

        # Insert foreground text
        self.svg.add(
            svgwrite.text.Text(
                insert=(
                    x.to_svg(),
                    (y + foreground_dy).to_svg()
                ),
                **self.foreground_text.to_svg_dict(),
                fill_opacity=1.0
            )
        )
