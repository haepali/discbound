import svgwrite
import random
import string

class Element(): # Formerly SvgContainer, jFormerly SvgGenerator
    """Element is a container class for a complex element in an SVG image.


    """
    def __init__(self, dx, dy, width, height, params):
        self.dx = dx
        self.dy = dy
        self.width = width
        self.height = height
        self.type = params["type"] # E.g.) dotgrid
        self.group = params["group"] # E.g.) grids, calendars, etc.
        self.raw_params = params["params"] # Decided at runtime
        self.svg = svgwrite.container.SVG(
            insert=(self.dx.to_svg(), self.dy.to_svg()),
            size=(self.width.to_svg(), self.height.to_svg())
        )

        self.unique_ids = {}

        # Inject Page number
        try:
            self.page_number = params["page_number"]
        except:
            pass

        # De-serialize the params
        self._load_params()
    
    def _load_params(self):
        # This needs to be overloaded
        self.params = self.raw_params

    def new_unique_id(self, basename):
        if basename in self.unique_ids:
            raise ValueError("UniqueID {} has already been registered".format(basename))

        rand_string = "".join(random.choice(string.ascii_letters) for i in range(10))
        new_id = basename + "_" + rand_string
        self.unique_ids[basename] = new_id

        return new_id

    def get_unique_id(self, basename):
        if basename not in self.unique_ids:
            raise ValueError("UniqueID {} has not been registered".format(basename))
        return self.unique_ids[basename]
