from collections import namedtuple

import numpy as np

from discbound.unit import Unit
from discbound.papersize import PaperSize


Imposition = namedtuple("Imposition", [
    "dimensions",
    "pages_per_sheet",
    "requires_rotation"
])


class _SheetIterator():

    def __init__(self, imposition, total_pages, dual_sided=True):
        dimensions = imposition.dimensions
        self.r = dimensions[0]
        self.c = dimensions[1]
        self.pages_per_sheet = imposition.pages_per_sheet
        self.total_pages = total_pages
        self.total_sheets = int(self.total_pages / self.pages_per_sheet)
        self.rotation = imposition.requires_rotation
        self.reverse_back_sheet = True # Selects whether to flip the page 
                                       # columns on the back of the sheet. This
                                       # should be left True for normal double 
                                       # sided print scenarios.

        # Constants
        self._constants()

        self.cur_sheet = 0

        self.front_pages = list(range(0, self.pages_per_sheet))
        self.back_pages = list(range(0, self.pages_per_sheet))

        # We need to reverse the 'columns' of the pages on the back so that they 
        # line up with the pages on the front.s
        if self.reverse_back_sheet:
            back_pages_np = np.reshape(self.back_pages, dimensions)
            back_pages_np = np.flip(back_pages_np, 1)
            self.back_pages = list(np.reshape(back_pages_np, self.pages_per_sheet))

        self.front_rotation = 0 if not self.rotation else 1
        self.back_rotation = 0 if not self.rotation else -1

    def _constants(self):
        pass

    def _pg_idx(self, idx, cur_side):
        return 0

    def __iter__(self):
        return self

    def __next__(self):

        if self.cur_sheet >= self.total_sheets:
            raise StopIteration

        cur_side = self.cur_sheet % 2

        if cur_side == 0:
            cur_pages = self.front_pages
            cur_rotation = self.front_rotation
        else:
            cur_pages = self.back_pages
            cur_rotation = self.back_rotation
        
        ret = ([self._pg_idx(i, cur_side) for i in cur_pages], cur_rotation)

        self.cur_sheet += 1

        return ret


class AccordionSheetIterator(_SheetIterator):
    """ An iterator which produces the following sequence of page indices.
    
        Front      Back
        +-------+  +-------+
        | 1 | 2 |  | 3 | 0 |
        +-------+  +-------+

        +-------+  +-------+
        | 5 | 6 |  | 7 | 4 |
        +-------+  +-------+
        ...
    """
    def _pg_idx(self, idx, cur_side):
        accordion_map = [
            [1, 2],
            [0, 3]
        ]

        cur_double_sheet = self.cur_sheet // 2
        pages_per_doule_sheet =  self.pages_per_sheet * 2
        pg_idx_sheet_offset = cur_double_sheet * pages_per_doule_sheet

        pg_idx = pg_idx_sheet_offset + accordion_map[cur_side][idx]

        return pg_idx


class StackSheetIterator(_SheetIterator):
    """ An iterator which produces the following sequence of page indices.
    
        Front      Back
        +-------+  +-------+
        | 0 | 4 |  | 5 | 1 |
        +-------+  +-------+

        +-------+  +-------+
        | 2 | 6 |  | 7 | 3 |
        +-------+  +-------+
        ...
    """
    def _pg_idx(self, idx, cur_side):
        return (idx * self.total_sheets) + self.cur_sheet


class MagazineStackSheetIterator(_SheetIterator):
    """ An iterator which produces the following sequence of page indices.

        Front      Back
        +-------+  +-------+
        | 1 | 6 |  | 7 | 0 |
        +-------+  +-------+

        +-------+  +-------+
        | 3 | 4 |  | 5 | 2 |
        +-------+  +-------+
        ...
    """
    def _constants(self):
        self.pages_per_stack_in_row = self.total_pages // self.r
        self.sub_pages_range = self.pages_per_stack_in_row

    def _pg_idx(self, idx, cur_side):
        cur_row = idx // self.c
        cur_col = idx % self.c

        sheet_offset = (cur_row * self.pages_per_stack_in_row)
        base_page_idx = (2 * (self.cur_sheet // 2)) + (1 - cur_side)
        left_page = base_page_idx + sheet_offset
        right_page = (self.sub_pages_range - base_page_idx - 1) + sheet_offset

        page_map = [left_page, right_page]

        return int(page_map[cur_col])

class ReaderViewSheetIterator(_SheetIterator):
    """ An iterator which produces the following sequence of page indices.
    
        Front      Back
        +-------+  +-------+
        | 0 | 1 |  | 2 | 3 |
        +-------+  +-------+

        +-------+  +-------+
        | 4 | 5 |  | 6 | 7 |
        +-------+  +-------+
        ...
    """

    def _constants(self):
        self.reverse_back_sheet = False

    def _pg_idx(self, idx, cur_side):
        return idx + (self.pages_per_sheet * self.cur_sheet)


def get_sheets(layout_type, imposition, num_pages, dual_sided=True):

    sheet_iterators = {
        "accordion": AccordionSheetIterator,
        "stack": StackSheetIterator,
        "magazine": MagazineStackSheetIterator,
        "reader": ReaderViewSheetIterator
    }

    sheet_iterator = sheet_iterators[layout_type](imposition, num_pages, dual_sided)

    return sheet_iterator


def get_imposition(page_size, page_orientation, sheet_size, sheet_orientation="portrait"):
    page_size = PaperSize(page_size, page_orientation)
    sheet_size = PaperSize(sheet_size, sheet_orientation)

    page_area = page_size.width * page_size.height
    sheet_area = sheet_size.width * sheet_size.height

    # Compare Normal
    normal_dimensions = (
        int((sheet_size.height / page_size.height).value),
        int((sheet_size.width / page_size.width).value)
    )
    num_normal = normal_dimensions[0] * normal_dimensions[1]

    # Compare Rotated
    rotated_dimensions = (
        int((sheet_size.height / page_size.width).value),
        int((sheet_size.width / page_size.height).value)
    )
    num_rotated = rotated_dimensions[0] * rotated_dimensions[1]

    if num_normal >= num_rotated:
        requires_rotation = False
        num_pages = num_normal
        dimensions = normal_dimensions
    else:
        requires_rotation = True
        num_pages = num_rotated
        dimensions = rotated_dimensions

    return Imposition(dimensions, num_pages, requires_rotation)


