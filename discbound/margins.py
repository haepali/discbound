from discbound.unit import Unit

_bleed_margins = {
    "default_metric": {
        "top": [4.3, "mm"],
        "left": [4.3, "mm"],
        "right": [4.3, "mm"],
        "bottom": [4.3, "mm"]
    },
    "default_imperial": {
        "top": [0.17, "in"],
        "left": [0.17, "in"],
        "right": [0.17, "in"],
        "bottom": [0.17, "in"]
    },
    "m252dw_windows_metric": {
        "top": [4.3, "mm"],
        "left": [4.3, "mm"],
        "right": [4.3, "mm"],
        "bottom": [4.3, "mm"]
    },
    "m252dw_linux_metric": {
        "top": [4.3, "mm"],
        "left": [4.3, "mm"],
        "right": [4.3, "mm"],
        "bottom": [12, "mm"]
    },
    "m252dw_windows_imperial": {
        "top": [0.17, "in"],
        "left": [0.17, "in"],
        "right": [0.17, "in"],
        "bottom": [0.17, "in"]
    },
    "m252dw_linux_imperial": {
        "top": [0.17, "in"],
        "left": [0.17, "in"],
        "right": [0.17, "in"],
        "bottom": [0.47, "in"]
    }
}

_punch_margins = {
    "default_imperial": [0.375, "in"]
}

class Margins():
    def __init__(self, bleed_margins="default_imperial", punch_margin_id="default_imperial", punch_edge="left", top=None, left=None, right=None, bottom=None):
        if bleed_margins == None:
            bleed_margins = "default_imperial"
        margins = _bleed_margins[bleed_margins]
        bleed_top = Unit(*margins["top"])
        bleed_left = Unit(*margins["left"])
        bleed_right = Unit(*margins["right"])
        bleed_bottom = Unit(*margins["bottom"])

        punch_margin = Unit(*_punch_margins[punch_margin_id])
        punch_top = punch_margin if punch_edge == "top" else Unit(0, "in")
        punch_left = punch_margin if punch_edge == "left" else Unit(0, "in")
        punch_right = punch_margin if punch_edge == "right" else Unit(0, "in")
        punch_bottom = punch_margin if punch_edge == "bottom" else Unit(0, "in")

        top = top if top is not None else Unit(0, "in")
        left = left if left is not None else Unit(0, "in")
        right = right if right is not None else Unit(0, "in")
        bottom = bottom if bottom is not None else Unit(0, "in")

        self.top = max(bleed_top, punch_top, top)
        self.left = max(bleed_left, punch_left, left)
        self.right = max(bleed_right, punch_right, right)
        self.bottom = max(bleed_bottom, punch_bottom, bottom)
