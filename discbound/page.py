
import svgwrite

from discbound.element import Element
from discbound.unit import Unit
from discbound.margins import Margins
from discbound.papersize import PaperSize
import discbound.contents as contents


_page_sizes = {
    "letter": {
        "width": [8.5, "in"],
        "height": [11, "in"]
    },
    "junior": {
        "width": [5.5, "in"],
        "height": [8.5, "in"]
    },
    "a5": {
        "width": [148, "mm"],
        "height": [210, "mm"]
    },
    "a4": {
        "width": [210, "mm"],
        "height": [297, "mm"]
    }
}


class PageParams():
    def __init__(self, params):
        self.size = params["size"]
        self.orientation = params["orientation"]
        self.punch_edge = params["punch_edge"]
        self.header_height = params.get("header_height", Unit(0, "in"))
        self.footer_height = params.get("footer_height", Unit(0, "in"))
        self.margins = params.get("margins", None)
        self.theme = params.get("theme", None)
        self.page_number = params.get("page_number", None)
        self.header = None
        self.footer = None
        self.body = None



class Page():
    def __init__(self, params):
        # Setup page dimensions
        page_size = PaperSize(params["size"], params["orientation"])
        self.width = page_size.width
        self.height = page_size.height
        self.svg = svgwrite.container.SVG(
            insert=(0, 0),
            size=(self.width.to_svg(), self.height.to_svg())
        )
        self.components = []

        # Get margins
        margins_id = params["margins"]
        punch_edge = params["punch_edge"]
        margins = Margins(margins_id, punch_edge=punch_edge)

        # Size / Offset tracking variables
        remaining_width = self.width - (margins.left + margins.right)
        remaining_height = self.height - (margins.top + margins.bottom)
        cur_dx = margins.left
        cur_dy = margins.top

        # Calculate the heights
        component_heights = {}
        for component in ["header", "footer", "body"]:
            try:
                component_heights[component] = Unit(*params[component + "_height"])
            except:
                component_heights[component] = Unit(0, "in")
            remaining_height -= component_heights[component]
        component_heights["body"] = remaining_height

        # Set up each component
        for component in ["header", "body", "footer"]:
            if params[component] is not None:
                contype = params[component]["type"]
                congroup = params[component]["group"]

                # Inject Page Number
                if params[component]["params"] is not None:
                    params[component]["page_number"] = params["page_number"]

                ContentElement = contents.content_factory(congroup, contype)
                self.components.append(
                    ContentElement(
                        dx = cur_dx,
                        dy = cur_dy,
                        height = component_heights[component],
                        width = remaining_width,
                        params = params[component]
                    )
                )
                cur_dy += component_heights[component]
            else:
                self.components.append(None)
                cur_dy += component_heights[component]


    def generate(self):
        for c in self.components:
            if c is not None:
                c.generate()
                self.svg.add(c.svg)
