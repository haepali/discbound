
from discbound.unit import Unit

_paper_sizes = {
    "halfjunior": {
        "width": [4.25, "in"],
        "height": [5.5, "in"]
    },
    "junior": {
        "width": [5.5, "in"],
        "height": [8.5, "in"]
    },
    "letter": {
        "width": [8.5, "in"],
        "height": [11, "in"]
    },
    "a5": {
        "width": [148, "mm"],
        "height": [210, "mm"]
    },
    "a4": {
        "width": [210, "mm"],
        "height": [297, "mm"]
    }
}


class PaperSize():
    def __init__(self, size, orientation="portrait"):
        _paper_size = _paper_sizes[size]

        self.width = Unit(*_paper_size["width"])
        self.height = Unit(*_paper_size["height"])
        self.orientation = orientation

        if ((self.orientation == "portrait" and self.width > self.height) or
            (self.orientation == "landscape" and self.height > self.width)):
            temp_w = self.width
            self.width = self.height
            self.height = temp_w


