import os
import sys
import svgwrite

from discbound.unit import Unit
from discbound.page import Page
from discbound.papersize import PaperSize


class Sheet():
    def __init__(self, size="letter", orientation="portrait", pages=None, flip=0, imposition=None):
        self.pages = pages
        if pages is None:
            self.pages = []
        else:
            self.pages = [Page(p) for p in pages]

        self.flip = flip
        self.imposition = imposition

        sheet_size = PaperSize(size, orientation)
        self.width = sheet_size.width
        self.height = sheet_size.height
        self.svg = svgwrite.container.SVG(
            insert=(0, 0),
            size=(self.width.to_svg(), self.height.to_svg())
        )


    def generate(self):
        i = 0
        num_rows, num_cols = self.imposition.dimensions
        pg_idx = 0
        for r in range(num_rows):
            for c in range(num_cols):
                p = self.pages[pg_idx]
                p.generate()

                if self.flip == 0:
                    xoffset = p.width * c
                    yoffset = p.height * r
                    rotation = 0
                elif self.flip == 1:
                    xoffset = p.height * (c+1)
                    yoffset = p.width * r
                    rotation = 90
                elif self.flip == -1:
                    xoffset = p.height * c
                    yoffset = p.width * (r+1)
                    rotation = -90
                else:
                    raise ValueError

                transform_container = svgwrite.container.Group(
                    transform="translate({dx} {dy}) rotate({rot})".format(
                        dx=xoffset.to_svg(True),
                        dy=yoffset.to_svg(True),
                        rot=rotation)
                )
                transform_container.add(p.svg)
                self.svg.add(transform_container)

                pg_idx += 1
