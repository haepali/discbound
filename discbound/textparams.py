
from discbound.unit import Unit

class TextParams():
    def __init__(self, params):
        self.text = params.get("text", "")
        self.text_anchor = params.get("text_anchor", "inherit")
        self.font_color = params.get("font_color", "black")
        self.font_family = params.get("font_family", "none")
        self.font_weight = params.get("font_weight", "normal")
        self.font_size = Unit(*params.get("font_size", [12, "px"]))
    
    def to_svg_dict(self):
        return {
            "text": self.text,
            "text_anchor": self.text_anchor,
            "fill": self.font_color,
            "font_family": self.font_family,
            "font_weight": self.font_weight,
            "font_size": self.font_size.to_svg()
        }