
class Unit():
    units_to_px = {
        "px": 1,
        "mm": (96 / 25.4),
        "cm": (96 / 2.54),
        "in": 96
    }

    def __init__(self, value, units, power=1):
        self.value = value
        self.units = units
        self.power = power

    def to_array(self):
        return [self.value, self.units, self.power]

    def to_svg(self, to_px=False):
        if to_px:
            return "{}".format(self._convert_to("px"))
        else:
            return "{}{}".format(self.value, self.units)

    def _to_px(self, value, units):
        return value * self.units_to_px[units]

    def _from_px(self, value, units):
        return value / self.units_to_px[units]

    def _convert_to(self, units):
        temp_val = self.value ** (1/self.power)
        temp_px = self._to_px(temp_val, self.units)
        temp_val = self._from_px(temp_px, units)
        temp_val = temp_val ** self.power

        return temp_val


    def convert_to(self, units):
        self.value = self._convert_to(units)
        self.units = units


    def __add__(self, other):
        if self.power != other.power:
            raise TypeError
        out = Unit(*other.to_array())
        out.convert_to(self.units)
        out.value += self.value
        return out

    def __sub__(self, other):
        if self.power != other.power:
            raise TypeError
        out = Unit(*self.to_array())
        out.convert_to(other.units)
        out.value -= other.value
        out.convert_to(self.units)
        return out

    def __mul__(self, other):
        if isinstance(other, Unit):
            temp = Unit(*other.to_array())
            temp.convert_to(self.units)
            out = Unit(*self.to_array())
            out.value *= temp.value
            out.power += temp.power
        else:
            out = Unit(*self.to_array())
            out.value *= other
        return out

    def __truediv__(self, other):
        if isinstance(other, Unit):
            temp = Unit(*other.to_array())
            temp.convert_to(self.units)
            out = Unit(*self.to_array())
            out.value /= temp.value
            out.power -= temp.power
        else:
            out = Unit(*self.to_array())
            out.value /= other
        return out

    def __floordiv__(self, other):
        out = Unit(*self.to_array())
        out.value = out.value // other
        return out

    def __neg__(self):
        out = Unit(*self.to_array())
        out.value = -out.value
        return out


    def __lt__(self, other):
        if self.power != other.power:
            raise TypeError
        temp = Unit(*other.to_array())
        temp.convert_to(self.units)
        return self.value < temp.value

    def __le__(self, other):
        if self.power != other.power:
            raise TypeError
        temp = Unit(*other.to_array())
        temp.convert_to(self.units)
        return self.value <= temp.value

    def __eq__(self, other):
        if self.power != other.power:
            raise TypeError
        temp = Unit(*other.to_array())
        temp.convert_to(self.units)
        return self.value == temp.value

    def __ne__(self, other):
        if self.power != other.power:
            raise TypeError
        temp = Unit(*other.to_array())
        temp.convert_to(self.units)
        return self.value != temp.value

    def __ge__(self, other):
        if self.power != other.power:
            raise TypeError
        temp = Unit(*other.to_array())
        temp.convert_to(self.units)
        return self.value >= temp.value

    def __ge__(self, other):
        if self.power != other.power:
            raise TypeError
        temp = Unit(*other.to_array())
        temp.convert_to(self.units)
        return self.value > temp.value
