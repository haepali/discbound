#!/usr/bin/python3
import os
import sys
import json
import svgwrite
import subprocess
import argparse

import discbound.booklet as booklet


_CUR_DIR = os.path.dirname(os.path.abspath(__file__))
_DEFAULT_BOOKLET_IN_DIR = os.path.join(_CUR_DIR, "booklet_collections")
_DEFAULT_BOOKLET_OUT_DIR = os.path.join(_CUR_DIR, "_out", "booklet_collections")


def get_booklet_type(bc_contents):
    """Introspects the booklet type 
    """
    if "pages" in bc_contents:
        return booklet.Booklet
    elif "booklet_config" in bc_contents:
        return booklet.BookletTemplateNamed
    elif any(k in bc_contents for k in ["header_id", "body_id", "footer_id"]):
        return booklet.BookletTemplateNamedParams
    else:
        return None


def load_booklet_collection(collection_name, booklet_collection):
    cur_collection = []

    for booklet_config in booklet_collection:
        booklet_type = get_booklet_type(booklet_config)
        booklet_config["collection"] = collection_name
        if booklet_type is not None:
            cur_collection.append({
                "type": booklet_type,
                "config": booklet_config,
                "collection": collection_name
            })

    return cur_collection


def load_booklet_collections(input_folder, booklet="all"):
    booklet_colletion_files = os.listdir(input_folder)

    booklet_configs = []

    # Each JSON is an entire collection of booklets, load each of them
    for bcf in booklet_colletion_files:
        if not bcf.endswith(".json"):
            continue

        booklet_collection_name = ".".join(bcf.split(".")[:-1])

        if booklet in ["all", booklet_collection_name]:
            with open(os.path.join(input_folder, bcf), mode="r") as bcf_file:
                bc_contents = json.load(bcf_file)
                booklet_configs += load_booklet_collection(
                    booklet_collection_name, 
                    bc_contents
                )

    return booklet_configs


def generate_svgs(booklet_configs, output_folder):
    for bc in booklet_configs:
        bc_config = bc["config"]
        bc_config["out_path"] = output_folder
        bc_config["custom_path_fragment"] = ""
        BCType = bc["type"]
        cur_booklet = BCType(bc_config)

        cur_booklet.generate()


def generate_booklet_sheet_pdfs(booklet_folder):
    """Generates individual sheet PDF's for the booklet in the specified folder.

    Inkscape is used to perform the SVG to PDF conversion. While not the fastest
    or most efficient conversion tool, it 1.) creates a smaller file size than 
    say ``rsvg-convert`` and 2.) it embeds fonts as paths
    """
    booklet_svg_files = os.listdir(booklet_folder)

    # Open inkscape in shell mode so that it's faster
    inkscape_proc = subprocess.Popen(
        ["inkscape", "--shell"],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    for svg_file in sorted(booklet_svg_files):
        if svg_file.endswith(".svg"):
            pdf_file = svg_file.rstrip(".svg") + ".pdf"
            svg_in_path = os.path.join(booklet_folder, svg_file)
            pdf_out_path = os.path.join(booklet_folder, pdf_file)
            print("Queueing PDF generation for {}".format(svg_in_path))
            # Add our current SVG to be converted
            inkscape_proc.stdin.write("--file={} --export-pdf={} --export-dpi=96 --export-text-to-path\n".format(
                svg_in_path, pdf_out_path
            ).encode())

    # Communicate with process
    out = inkscape_proc.communicate()
    inkscape_err = inkscape_proc.poll()
    if inkscape_err != 0:
        print("ERROR: failed to generate PDF's")


def generate_sheet_pdfs(output_folder):
    """Converts each SVG file into a PDF using Inkscape (currently)
    """
    booklet_folders = os.listdir(output_folder)
    for cur_folder in booklet_folders:
        cur_path = os.path.join(output_folder, cur_folder)
        if os.path.isdir(cur_path):
            generate_booklet_sheet_pdfs(cur_path)


def generate_booklet_merged_pdf(booklet_folder):
    pass

def generate_merged_pdfs(output_folder):
    """Combines each sheet PDF for each sheet into a single PDF for the booklet
    """
    booklet_folders = os.listdir(output_folder)
    for cur_folder in booklet_folders:
        cur_path = os.path.join(output_folder, cur_folder)
        if os.path.isdir(cur_path):
            # Get list of PDF files in each booklet folder
            cur_pdf_files = [f for f in os.listdir(cur_path) if f.endswith(".pdf")]
            cur_pdf_paths = [os.path.join(cur_path, pdf) for pdf in cur_pdf_files]
            cur_pdf_paths.sort()
            out_pdf = cur_path + ".pdf"
            pdfunite_command = ["pdfunite"] + cur_pdf_paths + [out_pdf]

            # Call `pdfunite` to merge them together
            subprocess.call(pdfunite_command)


def generate_booklet_menu_entry(booklet_config, output_folder):
    bc_config = booklet_config["config"]
    bc_config["out_path"] = output_folder
    bc_config["custom_path_fragment"] = ""
    BCType = booklet_config["type"]
    cur_booklet = BCType(bc_config)

    booklet_out_path = cur_booklet.booklet.base_path

    bc_group = bc_config.get("group", None)
    if bc_group is None:
        bc_group = bc_config.get("body_group")
    bc_id = bc_config.get("type", None)
    if bc_id is None:
        bc_id = bc_config.get("body_id")

    custom_path_fragment = ""
    if hasattr(cur_booklet, "custom_path_fragment"):
        custom_path_fragment = cur_booklet.custom_path_fragment

    theme = ""
    if hasattr(cur_booklet, "theme"):
        theme = cur_booklet.theme

    mapping_entry = {
        "group":        str(bc_group),
        "id":           str(bc_id),
        "collection":   str(cur_booklet.collection),
        "custom":       str(custom_path_fragment),
        "theme":        str(theme),
        "layout":       str(cur_booklet.layout),
        "page_size":    str(cur_booklet.page_size),
        "sheet_size":   str(cur_booklet.sheet_size),
        "base_path":    str(booklet_out_path),
        "thumbnail_path": str(os.path.join(booklet_out_path, "thumb.svg"))
    }

    return mapping_entry


def generate_menu_mapping_file(booklet_configs, output_folder):
    menu_file_name = "booklet_menu_file.json"
    menu_file_path = os.path.join(output_folder, menu_file_name)
    with open(menu_file_path, mode="w") as menu_file:
        booklet_config_menu_entries = []
        for bc in booklet_configs:
            booklet_config_menu_entries.append(generate_booklet_menu_entry(
                bc, output_folder
            ))
        json_str = json.dumps(booklet_config_menu_entries, sort_keys=True, indent=4)
        menu_file.write(json_str)


def generate(args):

    booklet_configs = load_booklet_collections(args.input_folder, args.booklet)

    generate_svgs(booklet_configs, args.output_folder)
    generate_menu_mapping_file(booklet_configs, args.output_folder)
    generate_sheet_pdfs(args.output_folder)
    generate_merged_pdfs(args.output_folder)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--booklet", dest="booklet", default="all", help="Specify the name of the booklet to generate")
    parser.add_argument("-i", "--input-folder", dest="input_folder", default=_DEFAULT_BOOKLET_IN_DIR, help="Input folder where booklets configs are located")
    parser.add_argument("-o", "--output-folder", dest="output_folder", default=_DEFAULT_BOOKLET_OUT_DIR, help="Output folder where booklets are generated to")
    args = parser.parse_args()

    generate(args)