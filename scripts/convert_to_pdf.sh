#!/bin/bash

# Quick one off script to convert the calendar SVG images into a single PDF for
# printing. Uses inkscape since that provided:
#
#   1. The most accurate output
#   2. Embedded fonts (or rather fonts converted to paths)


for filename in ./calendar/2019/*.svg; do
    echo "File: $filename"
    inkscape --export-pdf=$filename.pdf -f $filename -T
done

pdfunite calendar/2019/*.pdf calendar_2019.pdf
