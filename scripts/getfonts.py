#!/usr/bin/python3
"""Downloads list of specified fonts and extracts them to the users ~/.font 
directory.
"""
import os
import sys
import subprocess
import hashlib
import zipfile
import urllib

__CUR_DIR = os.path.dirname(os.path.realpath(__file__))
__TEMP_DIR = os.path.join(os.path.split(__CUR_DIR)[0], "_temp")
__USER_FONT_DIR = os.path.expanduser("~/.fonts")

class FontDownloader():
    def __init__(self, fontname, url, md5sum):
        self.fontname=fontname
        self.url=url
        self.md5sum=md5sum
    
    def get_font_package(self, download_dir):
        cur_checksum = ""
        file_path = os.path.join(download_dir, self.fontname)
        try:
            with open(file_path, mode="rb") as fontf:
                fontdata = fontf.read()
                m = hashlib.md5()
                m.update(fontdata)
                cur_checksum = m.hexdigest()
        except:
            pass
        
        if cur_checksum == self.md5sum:
            print("Already downloaded {}, skipping...".format(self.fontname))
        else:
            print("Downloading {} from {}".format(
                    self.fontname, 
                    self.url
            ))
            wget_call = ["wget", self.url, "-O", file_path]
            ret = subprocess.call(wget_call)
            if ret != 0:
                print("ERROR: failed to download {} from {}".format(
                    self.fontname, 
                    self.url
                ))

    def extract_font_package(self, from_dir, to_dir):
        """Extracts font to ~/.fonts"""
        zip_file = os.path.join(from_dir, self.fontname)
        output_path = os.path.join(to_dir, self.fontname.rstrip(".zip"))

        print("Extracting {} to {}".format(
            self.fontname,
            to_dir
        ))

        try:
            with zipfile.ZipFile(zip_file, 'r') as zip_ref:
                zip_ref.extractall(output_path)
        except:
            print("Failed to extract {} to {}".format(
                self.fontname,
                to_dir
            ))

_fonts = [
    FontDownloader(
        fontname="alex-brush.zip",
        url="https://fontlibrary.org/assets/downloads/alex-brush/0ac6d4d1fdda223fbbcd0640ced41b6a/alex-brush.zip",
        md5sum="0ac6d4d1fdda223fbbcd0640ced41b6a"
    ),
    FontDownloader(
        fontname="eb-garamond.zip",
        url="https://fontlibrary.org/assets/downloads/eb-garamond/ad76a14ea9b136557b474c3445caea08/eb-garamond.zip",
        md5sum="ad76a14ea9b136557b474c3445caea08"
    ),
    FontDownloader(
        fontname="Lobster_Two.zip",
        url="https://fonts.google.com/download?family=Lobster%20Two",
        md5sum="db152911db54fe9fedddfecf8ef38766"
    )
]

if __name__ == "__main__":
    temp_dir = ""
    os.makedirs(__TEMP_DIR, exist_ok=True)
    for font in _fonts:
        font.get_font_package(__TEMP_DIR)
        font.extract_font_package(__TEMP_DIR, __USER_FONT_DIR)