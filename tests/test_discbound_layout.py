#!/usr/bin/python3
import os
import sys
import json
import traceback

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

import discbound.sheet as sheet
import discbound.layout as layout
from discbound.layout import Imposition

total_passed = 0
total_failed = 0

def _test_accordion_iterator():
    global total_passed
    global total_failed

    test_cases = [
        {
            "imposition_params": ["junior", "portrait", "letter", "portrait"],
            "params": [8],
            "list_output": [
                ([1, 2], 1),
                ([0, 3], -1),
                ([5, 6], 1),
                ([4, 7], -1),
            ]
        },
        {
            "imposition_params": ["junior", "portrait", "letter", "landscape"],
            "params": [8],
            "list_output": [
                ([1, 2], 0),
                ([3, 0], 0),
                ([5, 6], 0),
                ([7, 4], 0),
            ]
        }
    ]

    for tc in test_cases:
        imposition_params = tc["imposition_params"]
        params = tc["params"]
        expected_output = tc["list_output"]

        imposition = layout.get_imposition(*imposition_params)
        params = [imposition] + params
        out = list(layout.AccordionSheetIterator(*params))
        print("Testing Accordion Layout: {}".format(params))
        if out == expected_output:
            total_passed += 1
        else:
            total_failed += 1
            print("Expected: {}".format(expected_output))
            print("Got: {}".format(out))


def _test_stack_iterator():
    global total_passed
    global total_failed

    test_cases = [
        {
            "imposition_params": ["junior", "portrait", "letter", "portrait"],
            "params": [8],
            "list_output": [
                ([0, 4], 1),
                ([1, 5], -1),
                ([2, 6], 1),
                ([3, 7], -1),
            ]
        },
        {
            "imposition_params": ["junior", "portrait", "letter", "landscape"],
            "params": [8],
            "list_output": [
                ([0, 4], 0),
                ([5, 1], 0),
                ([2, 6], 0),
                ([7, 3], 0),
            ]
        }
    ]

    for tc in test_cases:
        imposition_params = tc["imposition_params"]
        params = tc["params"]
        expected_output = tc["list_output"]

        imposition = layout.get_imposition(*imposition_params)
        params = [imposition] + params
        out = list(layout.StackSheetIterator(*params))

        print("Testing Stack Layout: {}".format(params))

        if out == expected_output:
            total_passed += 1
        else:
            total_failed += 1
            print("Expected: {}".format(expected_output))
            print("Got: {}".format(out))


def _test_magazine_iterator():
    global total_passed
    global total_failed

    test_cases = [
        {
            "imposition_params": ["junior", "portrait", "letter", "landscape"],
            "params": [8],
            "list_output": [
                ([1, 6], 0),
                ([7, 0], 0),
                ([3, 4], 0),
                ([5, 2], 0),
            ]
        },
        {
            "imposition_params": ["halfjunior", "portrait", "letter", "portrait"],
            "params": [24],
            "list_output": [
                ([1, 10, 13, 22], 0),
                ([11, 0, 23, 12], 0),
                ([3, 8, 15, 20], 0),
                ([9, 2, 21, 14], 0),
                ([5, 6, 17, 18], 0),
                ([7, 4, 19, 16], 0),
            ]
        }
    ]

    for tc in test_cases:
        imposition_params = tc["imposition_params"]
        params = tc["params"]
        expected_output = tc["list_output"]

        imposition = layout.get_imposition(*imposition_params)
        params = [imposition] + params
        out = list(layout.MagazineStackSheetIterator(*params))

        print("Testing Magazine Layout: {}".format(params))

        if out == expected_output:
            total_passed += 1
        else:
            total_failed += 1
            print("Expected: {}".format(expected_output))
            print("     Got: {}".format(out))


def _test_sheet_iterators():
    _test_accordion_iterator()
    _test_stack_iterator()
    _test_magazine_iterator()


def _test_imposition():
    global total_passed
    global total_failed

    test_cases = [
        {
            "params": ["junior", "portrait", "letter", "portrait"],
            "output": Imposition((2, 1), 2, True)
        },
        {
            "params": ["halfjunior", "portrait", "letter", "portrait"],
            "output": Imposition((2, 2), 4, False)
        },
        {
            "params": ["junior", "portrait", "letter", "landscape"],
            "output": Imposition((1, 2), 2, False)
        },
        {
            "params": ["halfjunior", "portrait", "letter", "landscape"],
            "output": Imposition((2, 2), 4, True)
        },
        {
            "params": ["a5", "portrait", "a4", "landscape"],
            "output": Imposition((1, 2), 2, False)
        },
        {
            "params": ["a5", "portrait", "a4", "portrait"],
            "output": Imposition((2, 1), 2, True)
        },
        {
            "params": ["a4", "portrait", "a5", "portrait"],
            "output": Imposition((0, 0), 0, False)
        }
    ]

    for tc in test_cases:
        params = tc["params"]
        expected_output = tc["output"]

        out = layout.get_imposition(*params)

        if out == expected_output:
            total_passed += 1
        else:
            total_failed += 1
            print("Expected: {}".format(expected_output))
            print("Got: {}".format(out))


def _test_all():
    _test_imposition()
    _test_sheet_iterators()

if __name__ == "__main__":
    print("="*80)
    _test_all()
    print("="*80)
    print("{}\{}".format(total_passed, total_passed + total_failed))
    exit(total_failed)