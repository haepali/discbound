#!/usr/bin/python3
import os
import sys
import json
import traceback
import xml.etree.ElementTree as et
import calendar
import svgwrite

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

import discbound.sheet as sheet
import discbound.layout as layout

total_passed = 0
total_failed = 0

class JsonConfigTestCase():
    def __init__(self, test_case, group_validate_function):
        self.test_module = sys.modules[__name__]
        if test_case["test_module"] is not None:
            self.test_module = __import__(
                    test_case["test_module"],
                    globals(), 
                    locals(), 
                    [test_case["test_object"]], 0)

        self.test_object = getattr(self.test_module, test_case["test_object"])
        self.test_passes = test_case["passes"]
        self.test_cases = test_case["test_cases"]
        try:
            self.validate_function = globals()[test_case["validate_function"]]
        except:
            self.validate_function = None
        self.group_validate_function = group_validate_function


    def run_test(self):
        pass_count = 0
        fail_count = 0

        for tc in self.test_cases:
            try:
                output = self.test_object(tc)

                if callable(self.validate_function):
                    passed = self.validate_function(output)
                    assert (passed == self.test_passes)

                if callable(self.group_validate_function):
                    passed = self.group_validate_function(output)
                    assert (passed == self.test_passes)

                if not self.test_passes:
                    fail_count += 1
                    print()
                    print("Unexpectedly passing!")
                    print(tc)
                    print()
                else:
                    pass_count += 1
            except Exception as e:
                if self.test_passes:
                    fail_count += 1
                    print()
                    print("Unexpectedly failing!")
                    traceback.print_exc()
                    print(tc)
                    print()
                else:
                    pass_count += 1

        return (pass_count, fail_count)


def _test_sheet(json_params):
    s = sheet.Sheet(
        **json_params,
        imposition=layout.get_imposition(
            json_params["pages"][0]["size"],
            json_params["pages"][0]["orientation"],
            json_params["size"],
            json_params["orientation"]
        )
    )

    return s


def _test_sheet_generate(sheet_obj):

    sheet_obj.generate()

    xml_str = sheet_obj.svg.tostring()
    pretty_xml = svgwrite.utils.pretty_xml(xml_str)
    os.makedirs("_out", exist_ok=True)
    f = open("_out/test_sheet_output.svg", mode="w")
    f.write(pretty_xml)
    f.close()

    return True


def _test_page_generate(page_obj):
    page_obj.generate()

    xml_str = page_obj.svg.tostring()
    pretty_xml = svgwrite.utils.pretty_xml(xml_str)
    os.makedirs("_out", exist_ok=True)
    f = open("_out/test_page_output.svg", mode="w")
    f.write(pretty_xml)
    f.close()

    return True


def _test_calendar_weekday_coloring(svg_output):
    """Checks that day cells are apporpriately colored for their weekday.
    """
    xml, idmap = et.XMLID(svg_output.svg.tostring())
    idkeys = [key for key in idmap.keys() if "calendar_cell_" in key]

    color_check_failed = False 

    for idkey in idkeys:
        element = idmap[idkey]
        year, month, day = map(int, idkey.split("_")[-1].split("-"))
        expected_colors = [
            "white", "white", "white", "white", "white", #weekday
            "paleturquoise", "paleturquoise", # weekend
            "lightgrey" # out of range
        ]
        try:
            weekday = calendar.weekday(year, month, day)
        except:
            weekday = -1
        
        color_e = element.getchildren()[1] # Second child element is our color square

        expected_color = expected_colors[weekday]
        cur_color = color_e.attrib["fill"]
        if cur_color != expected_color:
            print("Failed color check for {}, expected {} got {}".format(
                idkey,
                expected_color,
                cur_color
            ))
            color_check_failed = True
    
    return not color_check_failed


def _test_calendar_weekday_alignment(svg_output):
    """For grid calendars, checks that days are aligned vertically by weekday.
    """
    xml, idmap = et.XMLID(svg_output.svg.tostring())
    idkeys = [key for key in idmap.keys() if "calendar_cell_" in key]

    # Build up weekday map of {weekday: [element, [element]]}
    elements_by_weekday = {}
    for idkey in idkeys:
        try:
            year, month, day = map(int, idkey.split("_")[-1].split("-"))
            weekday = calendar.weekday(year, month, day)
            cur_items = elements_by_weekday.get(weekday, [])
            cur_items.append(idmap[idkey])
            elements_by_weekday[weekday] = cur_items
        except:
            pass

    # All weekdays should be aligned vertically
    alignment_failed = False
    for weekday, elements in elements_by_weekday.items():
        expected_x = elements[0].attrib["x"]
        for e in elements:
            cur_x = e.attrib["x"]

            if cur_x != expected_x:
                alignment_failed = True
                print("Failed alignment for 'id': {}. Expected 'x': {} got 'x': {}".format(
                    e.attrib["id"],
                    expected_x,
                    cur_x
                ))

    return not alignment_failed



def test_calendar_date_ranges(svg_output):
    global total_tests
    global total_fails

    svg_output.generate()

    # Validate weekday alignment
    alignment_passed = _test_calendar_weekday_alignment(svg_output)
    color_passed = _test_calendar_weekday_coloring(svg_output)

    return alignment_passed


def _test_booklet_generate(booklet):
    """
    """
    booklet.generate()

    return True

def _test_json_files(json_test_folder, group_validate_function):
    global total_passed
    global total_failed

    cur_test_folder = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "json_test_files",
        json_test_folder
    )

    json_test_files = os.listdir(cur_test_folder)
    for test_file in json_test_files:

        if not test_file.endswith(".json"):
            continue

        print("Testing: {}".format(os.path.join(cur_test_folder, test_file)))

        with open(os.path.join(cur_test_folder, test_file), mode="r") as jsonf:
            json_contents = json.load(jsonf)
            json_test = JsonConfigTestCase(json_contents, group_validate_function)

            test_pass, test_fail = json_test.run_test()

            total_passed += test_pass
            total_failed += test_fail

        print()



def _test_all_json_files():
    _test_json_files("contents", None)
    _test_json_files("page", _test_page_generate)
    _test_json_files("sheet", _test_sheet_generate)
    _test_json_files("booklet_generate", _test_booklet_generate) # TODO: add back in booklet generation


if __name__ == "__main__":
    print("="*80)
    _test_all_json_files()
    print("="*80)
    print("{}\{}".format(total_passed, total_passed + total_failed))
    exit(total_failed)

